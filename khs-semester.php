<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Kartu Hasil Studi</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Kartu Hasil Studi
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun Akademik/Semester</td>
                                                    <td>:</td>
                                                    <td>2015/2016 Ganjil - 1</td>
                                                </tr>
                                            </table>
                                        </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">

                                        <div class="card-header">
                                            Nilai Mata Kuliah Teori
                                            <div class="btn-actions-pane-right">
                                                <button class="btn-transition btn btn-outline-primary"><i class="pe-7s-print"></i> Print KHS</button>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode MK</th>
                                                    <th>Mata Kuliah</th>
                                                    <th>SKS</th>
                                                    <th>Nilai</th>
                                                    <th>Nilai Huruf</th>
                                                    <th>Detail</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>MPK-1202</td>
                                                        <td>Kimia Dasar</td>
                                                        <td>2(0)</td>
                                                        <td>81</td>
                                                        <td>A</td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="detail-khs-teori.php"><i class="pe-7s-look"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>MPK-1202</td>
                                                        <td>Fisika Dasar</td>
                                                        <td>3(1)</td>
                                                        <td>81</td>
                                                        <td>A</td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="detail-khs.php"><i class="pe-7s-look"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                        <div class="card-header">
                                            Nilai Mata Kuliah Praktikum
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode MK</th>
                                                    <th>Mata Kuliah</th>
                                                    <th>SKS</th>
                                                    <th>Nilai</th>
                                                    <th>Nilai Huruf</th>
                                                    <th>Detail</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>MPK-1202</td>
                                                        <td>Fisika Dasar</td>
                                                        <td>3(1)</td>
                                                        <td>81</td>
                                                        <td>A</td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="detail-khs-praktikum.php"><i class="pe-7s-look"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
