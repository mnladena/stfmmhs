<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php" ?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Menu Utama</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Halaman Utama</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Halaman Utama
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <!-- <div class="col-md-6 col-xl-7">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Kelas terdekat saat ini</div>
                                        <div class="card-body kelas">
                                            <h5>Perkembangan Farmasi</h5>
                                            <table class="mb-0 table table-borderless">
                                                
                                                <tr>
                                                    <td>Pertemuan 2</td>
                                                    <td>:</td>
                                                    <td>Klasifikasi Bidang kefarmasian</td>
                                                </tr>
                                                <tr>
                                                    <td>Pengajar</td>
                                                    <td>:</td>
                                                    <td>Prof. Hendrawan, S.Farm</td>
                                                </tr>
                                                <tr>
                                                    <td>Ruang</td>
                                                    <td>:</td>
                                                    <td>H.102</td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div> -->

                                
                                <!-- <div class="col-md-6 col-xl-5">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Kelas Selanjutnya</div>
                                        <div class="card-body">
                                            <div class="scroll-area-sm">
                                                <div class="scrollbar-container ps--active-y">
                                                   <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <h5 class="list-group-item-heading">Farmakologi Dasar</h5>
                                                            <table class="mb-0 table table-borderless">
                                                        
                                                                <tr>
                                                                    <td>Pertemuan</td>
                                                                    <td>:</td>
                                                                    <td>3</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Ruang</td>
                                                                    <td>:</td>
                                                                    <td>H.102</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Waktu</td>
                                                                    <td>:</td>
                                                                    <td>14.00 - 15.00</td>
                                                                </tr>
                                                                
                                                            </table>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <h5 class="list-group-item-heading">Pengantar Biologi Dasar</h5>
                                                            <table class="mb-0 table table-borderless">
                                                        
                                                                <tr>
                                                                    <td>Pertemuan</td>
                                                                    <td>:</td>
                                                                    <td>3</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Ruang</td>
                                                                    <td>:</td>
                                                                    <td>H.102</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Waktu</td>
                                                                    <td>:</td>
                                                                    <td>14.00 - 15.00</td>
                                                                </tr>
                                                                
                                                            </table>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <h5 class="list-group-item-heading">Farmakologi</h5>
                                                            <table class="mb-0 table table-borderless">
                                                                <tr>
                                                                    <td>Pertemuan</td>
                                                                    <td>:</td>
                                                                    <td>3</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Ruang</td>
                                                                    <td>:</td>
                                                                    <td>H.102</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Waktu</td>
                                                                    <td>:</td>
                                                                    <td>14.00 - 15.00</td>
                                                                </tr>
                                                                
                                                            </table>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <h5 class="list-group-item-heading">Pengantar Biologi Dasar</h5>
                                                            <table class="mb-0 table table-borderless">
                                                        
                                                                <tr>
                                                                    <td>Pertemuan</td>
                                                                    <td>:</td>
                                                                    <td>3</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Ruang</td>
                                                                    <td>:</td>
                                                                    <td>H.102</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Waktu</td>
                                                                    <td>:</td>
                                                                    <td>14.00 - 15.00</td>
                                                                </tr>
                                                                
                                                            </table>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Berita/Pengumuman</div>
                                         <div class="card-body title-text">
                                            <a href="berita.php">
                                                <span class="title-text__date">21 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">22 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">21 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">22 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">21 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                                            </a>


                                         <nav class="mt-3" aria-label="page navigation">
                                                <ul class="pagination">
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                                                    <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
