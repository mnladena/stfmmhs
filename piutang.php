<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Keuangan</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Piutang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Daftar Hutang
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Jenis Bayar</th>
                                                    <th>TA/Semester</th>
                                                    <th>Total Biaya</th>
                                                    <th>Potongan</th>
                                                    <th>Sudah Bayar</th>
                                                    <th>Piutang</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>BPP</td>
                                                        <td>
                                                            <div>2015/2016 </div>
                                                            <div>Semester 1</div>
                                                        </td>
                                                        <td>Rp 10.000.000</td>
                                                        <td>Rp 30.000</td>
                                                        <td>Rp 5.000.000</td>
                                                        <td>Rp 5.000.000</td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="bayar-tagihan.php"><i class="pe-7s-look"></i> Bayar</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Cuti</td>
                                                        <td>
                                                            <div>2015/2016 </div>
                                                            <div>Semester 1</div>
                                                        </td>
                                                        <td>Rp 10.000.000</td>
                                                        <td>Rp 30.000</td>
                                                        <td>Rp 5.000.000</td>
                                                        <td>Rp 5.000.000</td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="bayar-tagihan.php"><i class="pe-7s-look"></i> Bayar</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
