<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Kartu Rencana Studi</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Kartu Rencana Studi
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                            </table>
                                        </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card card">
                                    
                                    <div class="card-header">
                                        <ul class="nav">
                                            <li class="nav-item"><a data-toggle="tab" href="#krs-aktif" class="<?php if(isset($_GET['isi'])==1 or isset($_GET['batal'])==1 )
                                                        { } 
                                                        else { 
                                                            echo'active';
                                                        }?>   nav-link">KRS Aktif</a></li>
                                            <li class="nav-item"><a data-toggle="tab" href="#krs-isi" class="<?php if(isset($_GET['isi'])==1 or isset($_GET['batal'])==1 )
                                                        {  echo'active'; } 
                                                        else { }?>  nav-link">Pengisian KRS</a></li>
                                        </ul>
                                    </div>
                                    
                                    <div class="card-body">

                                        <div class="tab-content">

                                            <div class="tab-pane <?php if(isset($_GET['isi'])==1 or isset($_GET['batal'])==1)
                                                        { } 
                                                        else { 
                                                            echo'active';
                                                        }?>  " id="krs-aktif" role="tabpanel">

                                                <div class="main-card mb-3 card card-border">
                                                    <div class="card-header">
                                                        List KRS Aktif
                                                    </div>
                                                    <div class="card-body">
                                                        <?php if(isset($_GET["empty"])==1)
                                                        {?>
                                                            <div class="alert alert-primary text-center">
                                                                Tidak ada KRS Aktif
                                                            </div>
                                                       <?php } 
                                                        else {?> 
                                                        <div class="table-responsive">
                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Kode Matkul</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Jenis Mata Kuliah</th>
                                                                <th>SKS</th>
                                                                <th>Kelas</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1 (Kemanusiaan dan Keimanan)</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1203</td>
                                                                    <td>Kimia Dasar</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1204</td>
                                                                    <td>Fisika Dasar</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1205</td>
                                                                    <td>Biologi Sel</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1206</td>
                                                                    <td>Praktikum Biologi Sel</td>
                                                                    <td>Praktek</td>
                                                                    <td>2</td>
                                                                    <td>A</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                        <?php } ?>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="tab-pane <?php if(isset($_GET['isi'])==1 or isset($_GET['batal'])==1)
                                                        {
                                                            echo'active';
                                                        } 
                                                        else { }?> " id="krs-isi" role="tabpanel">

                                                <div class="main-card mb-3 card card-border">

                                                    <?php if(isset($_GET["isi"])==1){ ?>

                                                    <div class="card-header">
                                                        KRS Tambahan Tersedia
                                                    </div>

                                                    <div class="card-body">
                                                        <form>
                                                        <div class="table-responsive">
                                                        <table class="align-middle text-center mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Jenis Mata Kuliah</th>
                                                                <th>SKS</th>
                                                                <th>Kelas</th>
                                                                <th>Slot</th>
                                                                <th>Pilih</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>MPK-1202</td>
                                                                    <td>Kimia Dasar</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>
                                                                        <div class="position-relative">
                                                                            <select type="select" id="exampleCustomSelect" name="customSelect" class="custom-select">
                                                                                <option value="">A</option>
                                                                                <option>B</option>
                                                                                <option>C</option>
                                                                                <option>D</option>
                                                                                <option>E</option>
                                                                                <option>F</option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td>8</td>
                                                                    <td>
                                                                        <div class="custom-checkbox custom-control">
                                                                            <input type="checkbox" id="check1" class="custom-control-input">
                                                                            <label class="custom-control-label" for="check1"></label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1203</td>
                                                                    <td>Fisika Dasar</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>
                                                                        <div class="position-relative">
                                                                            <select type="select" id="exampleCustomSelect" name="customSelect" class="custom-select">
                                                                                <option value="">A</option>
                                                                                <option>B</option>
                                                                                <option>C</option>
                                                                                <option>D</option>
                                                                                <option>E</option>
                                                                                <option>F</option>
                                                                            </select>
                                                                        </div>
                                                                    </td>
                                                                    <td>10</td>
                                                                    <td>
                                                                        <div class="custom-checkbox custom-control">
                                                                            <input type="checkbox" id="check2" class="custom-control-input">
                                                                            <label class="custom-control-label" for="check2"></label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                        <div class="text-center mt-4">
                                                            <a href="krs.php?batal=1" class="btn btn-secondary">Batal</a>
                                                            <a href="" data-toggle="modal" data-target=".simpan-krs" class="btn btn-primary">Simpan</a>
                                                        </div>
                                                        
                                                        </form>
                                                    </div>
                                                           
                                                    <?php } else {?> 

                                                    <h5 class="card-title ml-sm-4 mt-sm-4">KRS Tambahan Tersedia</h5>
                                                    <div class="card-header action">
                                                            <div class="alert alert-primary text-center">Waktu Pengisian KRS: 20 Mei 2020 |  10.00-11.00</div>    
                                                            <div class="btn-actions-pane-right">
                                                                <a href="krs.php?isi=1" class="btn-transition btn btn-primary"><i class="pe-7s-pen"></i> Isi KRS</a>
                                                            </div>
                                                    </div>    

                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Jenis Mata Kuliah</th>
                                                                <th>SKS</th>
                                                                <th>Slot Tersedia</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>MPK-1202</td>
                                                                    <td>Kimia Dasar</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>8</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1203</td>
                                                                    <td>Fisika Dasar</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>4</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1207</td>
                                                                    <td>Praktikum Fisika Dasar</td>
                                                                    <td>Praktek</td>
                                                                    <td>2</td>
                                                                    <td>9</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                        
                                                    </div>

                                                    <div class="card-header">
                                                        KRS Tambahan Diambil
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Jenis Mata Kuliah</th>
                                                                <th>SKS</th>
                                                                <th>Kelas</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>MPK-1202</td>
                                                                    <td>Fisika Dasar</td>
                                                                    <td>Teori</td>
                                                                    <td>2</td>
                                                                    <td>A</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </div>
                                                    </div>

                                                    <?php } ?>

                                                </div>

                                        
                                            
                                        </div>
                                        
                                    </div>

                                </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>

                    <!-- simpan perubahan -->
                    <div class="modal fade simpan-krs" tabindex="-1" role="dialog" aria-labelledby="daftarRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Konfirmasi</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <div class="text-center">
                                        Apakah Anda yakin dengan pilihan Anda?
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    
            