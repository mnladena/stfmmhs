<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Keuangan</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Tagihan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Tagihan Keuangan
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Proses Pembayaran
                                        </div>
                                        <div class="card-body">

                                            <div class="alert alert-danger">
                                               Bukti bayar yang diupload tidak sesuai dengan nilai yang harus ditransfer
                                            </div>

                                            <form>
                                            <div class="row">

                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Biaya Remedial</div>
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt"><b>Rp 1.100.000</b>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="text-primary"><b>Catatan:</b> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                                                    </div>

                                                    <div class="col-md-12"><hr></div>

                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Total Biaya</div>
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt"><b>Rp 1.100.000</b>
                                                    </div>

                                                    <div class="col-md-9 pt-3 pb-3 tb-title">
                                                        Nilai yang harus ditransfer
                                                        <div class="text-danger"><small>*Harap mentransfer sesuai dengan nilai yang tertulis agar tidak menghambat proses verifikasi</small></div>
                                                    </div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">
                                                        <span class="card text-white card-body bg-info inline-block"><h3 class="text-white card-title">Rp 13.500.000</h3></span>
                                                    </div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Pilihan Bank Transfer</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">
                                                        
                                                            <select class="form-control">
                                                                <option>List Bank</option>
                                                                <option>BCA</option>
                                                                <option>BNI</option>
                                                                
                                                            </select>
                                                        
                                                    </div>
                                                
                                                    <div class="col-md-7 pt-3 pb-3 tb-title">Bukti Bayar</div>
                                                    
                                                    <div class="col-md-5 pt-3 pb-3 tb-cnt">
                                                        <div id="previewupload"></div>
                                                    </div>
                                                
                                               
                                            
                                            <div class="col-md-12 pt-3 pb-3 tb-title">Rekening Tujuan</div>
                                                <div class="col-md-6">
                                                    <div class="card-border mb-3 card regular card-body border-focus text-center">
                                                        <img class="img-md" src="assets/images/bni.png" alt="">
                                                        <div class="text-center mt-2">
                                                            Bank BNI, Tigaraksa
                                                            <div class="text big-text text-primary">999.7873.2346</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card-border mb-3 card regular card-body border-focus text-center">
                                                        <img class="img-md" src="assets/images/bca.png" alt="">
                                                        <div class="text-center mt-2">
                                                            Bank BCA, Tigaraksa
                                                            <div class="text big-text text-primary">999.7873.2346</div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="alert alert-warning">
                                               Proses verifikasi memerlukan waktu 1x24jam
                                            </div>

                                            <div class="text-center">
                                                    <button class="btn btn-primary" type="submit">Kirim</button>
                                                    <a class="btn btn-secondary" href="tagihan.php">Kembali</a>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
