<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Sidang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    SKRIPSI
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">

                                            <h3 class="text-center mb-4">Berkas Proposal Skripsi/KTI</h3>
                                            <hr>
                                            <div class="table-responsive">
                                                <table class="align-baseline mb-0 table table-data table-borderless">
                                                    <tr>
                                                        <td>Judul Skripsi</td> 
                                                        <td>:</td>
                                                        <td>
                                                            Pembuatan Obat Herbal Tradisional
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dosen Pembimbing</td> 
                                                        <td>:</td>
                                                        <td>
                                                            <ol class="none">
                                                                <li class="mb-3">Dr. Syamsul Fuadi</li>
                                                                <li class="mb-3">Dr. Raihan Fuadi</li>
                                                            </ol>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Proposal Skripsi</td> 
                                                        <td>:</td>
                                                        <td>
                                                            <button class="mb-2 mr-2 btn btn-info"><span class="pe-7s-file"></span> Proposal</button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Seminar Proposal</td> 
                                                        <td>:</td>
                                                        <td>
                                                            <button class="mb-2 mr-2 btn btn-info"><span class="pe-7s-file"></span> Seminar Proposal</button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>

                                
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                     
