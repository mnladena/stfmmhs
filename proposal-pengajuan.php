<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Sidang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    SKRIPSI
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">

                                            <h3 class="text-center mb-4">Formulir Pengajuan Proposal Skripsi</h3>
                                            <hr>
                                            <form class="needs-validation" novalidate>

                                            <fieldset class="position-relative row form-group mt-5 mb-5">
                                                <legend class="col-form-label col-sm-12">Judul/Topik Penelitian</legend>
                                                <div class="col-sm-12">
                                                    <div class="position-relative row form-group">
                                                        <label for="exampleEmail" class="col-sm-2 text-center col-form-label">1</label>
                                                        <div class="col-sm-10">
                                                            <input name="judul1" id="" placeholder="" type="text" class="form-control" required>
                                                            <div class="invalid-feedback">
                                                                Judul harus diisi
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="position-relative row form-group">
                                                        <label for="exampleEmail" class="col-sm-2 text-center col-form-label">2</label>
                                                        <div class="col-sm-10">
                                                            <input name="judul2" id="" placeholder="" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <fieldset class="position-relative row form-group">
                                                <legend class="col-form-label col-sm-12">Dosen Pembimbing</legend>
                                                <div class="col-sm-12">
                                                    <div class="position-relative row form-group">
                                                        <label for="exampleEmail" class="col-sm-2 text-center col-form-label">1</label>
                                                        <div class="col-sm-10">
                                                            <select name="select" id="" class="form-control" required>
                                                                <option value="">--List Dosen--</option>
                                                                <option>Drs. Wahyudi</option>
                                                                <option>Prof. Firman Son</option>
                                                                <option>Dr. Itani Mulli</option>
                                                            </select>
                                                            <div class="invalid-feedback">
                                                                Judul harus diisi
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="position-relative row form-group">
                                                        <label for="exampleEmail" class="col-sm-2 text-center col-form-label">2</label>
                                                        <div class="col-sm-10">
                                                            <select name="select" id="" class="form-control" required>
                                                                <option value="">--List Dosen--</option>
                                                                <option>Drs. Wahyudi</option>
                                                                <option>Prof. Firman Son</option>
                                                                <option>Dr. Itani Mulli</option>
                                                            </select>
                                                            <div class="invalid-feedback">
                                                                Judul harus diisi
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="position-relative row form-group">
                                                        <label for="" class="col-sm-2 text-center col-form-label">3</label>
                                                        <div class="col-sm-10">
                                                            <select name="select" id="" class="form-control">
                                                                <option>--List Dosen--</option>
                                                                <option>Drs. Wahyudi</option>
                                                                <option>Prof. Firman Son</option>
                                                                <option>Dr. Itani Mulli</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <div class="alert alert-info">
                                                <strong>Catatan</strong>
                                                <ol>
                                                    <li>
                                                        Judul/topik bisa diajukan lebih dari 1 topik
                                                    </li>
                                                    <li>
                                                        Dosen pembimbing yang diajukan bisa lebih dari 2
                                                    </li>
                                                </ol>
                                            </div>

                                            <div class="position-relative row form-check">
                                                <div class="text-center">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </div>
                                        </form>

                                        <script>
                                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                                        (function() {
                                            'use strict';
                                            window.addEventListener('load', function() {
                                                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                                var forms = document.getElementsByClassName('needs-validation');
                                                // Loop over them and prevent submission
                                                var validation = Array.prototype.filter.call(forms, function(form) {
                                                    form.addEventListener('submit', function(event) {
                                                        if (form.checkValidity() === false) {
                                                            event.preventDefault();
                                                            event.stopPropagation();
                                                        }
                                                        form.classList.add('was-validated');
                                                    }, false);
                                                });
                                            }, false);
                                        })();
                                    </script>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>

                                
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                     
