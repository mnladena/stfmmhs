<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Penilaian Mata Kuliah
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Salahudin Yussuf - 1101016431</td>
                                                </tr>
                                                <tr>
                                                    <td>Mata Kuliah</td>
                                                    <td>:</td>
                                                    <td>Pengantar Farmasi</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab1" class="active nav-link">Keaktifan</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab2" class="nav-link">Pre-Post Test</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab3" class="nav-link">Laporan</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                
                                                <!-- keaktifan -->
                                                <div class="tab-pane active" id="tab1" role="tabpanel">
                                                    
                                                    <div class="card-header">
                                                        Nilai Keaktifan

                                                        <div class="btn-actions-pane-right">
                                                            <button class="btn-transition btn btn-outline-primary"><i class="pe-7s-download"></i> Download Penilaian</button>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Pertemuan</th>
                                                                <th>Tugas</th>
                                                                <th>Catatan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    1
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    2
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   3
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    4
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    5
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    6
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   7
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    8
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   9
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>
                                                            <tfoot class="total">
                                                                <tr>
                                                                    <td>Rata-rata</td>
                                                                    <td colspan="2">75</td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                        
                                                    </div>

                                                </div>
                                                <!-- keaktifan -->

                                                <!-- pre-post test -->
                                                <div class="tab-pane" id="tab2" role="tabpanel">

                                                    <div class="card-header">
                                                        Nilai Pre-Post Test

                                                        <div class="btn-actions-pane-right">
                                                            <button class="btn-transition btn btn-outline-primary"><i class="pe-7s-download"></i> Download Penilaian</button>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Pertemuan</th>
                                                                <th>Tugas</th>
                                                                <th>Catatan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    1
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    2
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   3
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    4
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    5
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    6
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   7
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    8
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   9
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>
                                                            <tfoot class="total">
                                                                <tr>
                                                                    <td>Rata-rata</td>
                                                                    <td colspan="2">75</td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                        
                                                    </div>
                                                    
                                                </div>
                                                <!-- pre-post test -->
                                                
                                                <!-- laporan -->
                                                <div class="tab-pane" id="tab3" role="tabpanel">

                                                   <div class="card-header">
                                                        Nilai Laporan

                                                        <div class="btn-actions-pane-right">
                                                            <button class="btn-transition btn btn-outline-primary"><i class="pe-7s-download"></i> Download Penilaian</button>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Pertemuan</th>
                                                                <th>Tugas</th>
                                                                <th>Catatan</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    1
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    2
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   3
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    4
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    5
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    6
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   7
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    8
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                   9
                                                                </td>
                                                                <td>
                                                                    75
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>
                                                            <tfoot class="total">
                                                                <tr>
                                                                    <td>Rata-rata</td>
                                                                    <td colspan="2">75</td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                        
                                                    </div>

                                                </div>
                                                <!-- laporan -->

                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
