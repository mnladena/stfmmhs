<div class="app-sidebar sidebar-shadow">

                    <div class="app-header__logo">
                        <div class="logo-src">
                            <img src="assets/images/logo.png">
                            
                        </div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>

                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>   

                    <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li class="app-sidebar__heading">Menu Utama</li>
                                <li>
                                    <a href="index.php" class="mm-active">
                                        <i class="metismenu-icon pe-7s-home"></i>
                                        Halaman Utama
                                    </a>
                                </li>
                                <li class="app-sidebar__heading">Akademik</li>
                                <li>
                                    <a href="krs.php">
                                        <i class="metismenu-icon pe-7s-note2"></i>
                                       Kartu Rencana Studi
                                    </a>
                                </li>
                                <li>
                                    <a href="perkuliahan.php">
                                        <i class="metismenu-icon pe-7s-notebook"></i>
                                       Perkuliahan
                                    </a>
                                </li>
                                <li>
                                    <a href="ujian.php">
                                        <i class="metismenu-icon pe-7s-note"></i>
                                        Ujian
                                    </a>
                                </li>
                                <li>
                                    <a href="khs.php">
                                        <i class="metismenu-icon pe-7s-note2"></i>
                                        Kartu Hasil Studi
                                    </a>
                                </li>
                                <li>
                                    <a href="sidang-aik.php">
                                        <i class="metismenu-icon pe-7s-display2"></i>
                                       Sidang
                                       <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="sidang-aik.php">AIK</a>
                                        </li>
                                        <li>
                                            <a href="sidang-proposal.php">Proposal Skripsi</a>
                                        </li>
                                        <li>
                                            <a href="sidang-skripsi.php">Skripsi</a>
                                        </li>
                                        <li>
                                            <a href="sidang-komprehensif.php">Komprehensif</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="prakerja.php">
                                        <i class="metismenu-icon pe-7s-portfolio"></i>
                                        Pra-Kerja
                                    </a>
                                </li>
                                <li>
                                    <a href="bimbingan.php">
                                        <i class="metismenu-icon pe-7s-help2"></i>
                                        Bimbingan Akademik
                                    </a>
                                </li>
                                <li class="app-sidebar__heading">Layanan</li>
                                <li>
                                    <a href="#">
                                        <i class="metismenu-icon pe-7s-display2"></i>
                                        Layanan Menu
                                    </a>
                                </li>
                                <li class="app-sidebar__heading">Keuangan</li>
                                <li>
                                    <a href="tagihan.php">
                                        <i class="metismenu-icon pe-7s-mouse"></i>
                                        Tagihan
                                    </a>
                                </li>
                                 <li>
                                    <a href="piutang.php">
                                        <i class="metismenu-icon pe-7s-mouse"></i>
                                        Hutang
                                    </a>
                                </li>
                                 <li>
                                    <a href="riwayat.php">
                                        <i class="metismenu-icon pe-7s-mouse"></i>
                                        Riwayat
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>  