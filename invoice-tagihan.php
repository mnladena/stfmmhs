<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Keuangan</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Tagihan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Tagihan Keuangan
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                           Konfirmasi Proses Pembayaran
                                        </div>
                                        <div class="card-body">

                                            <div class="row">

                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Biaya Daftar Ulang</div>
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt"><b>Rp 1.100.000</b>
                                                    </div>

                                                    <div class="col-md-12"><hr></div>

                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">BOP (Biaya Opersional Perkuliahan)</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Rp 14.100.000</div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Diskon (IPK: 3.5 - 50%)</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Rp 7.100.000</div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Cara Pembayaran</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Cicilan 2x Bayar</div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Biaya yang akan dibayar</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt"><b>Rp 7.100.000</b></div>
                                                
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Sisa Pembayaran</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Rp 7.100.000</div>
                                                    
                                                    <div class="col-md-12"><hr></div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">BPP (Bangunan)</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Rp 14.100.000</div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Diskon (Kader: 50%)</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Rp 7.100.000</div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Cara Pembayaran</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Cicilan 2x Bayar</div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Biaya yang akan dibayar</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt"><b>Rp 7.100.000</b></div>
                                                
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Sisa Pembayaran</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">Rp 7.100.000</div>

                                                    <div class="col-md-12"><hr></div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Total Biaya</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt"><b>Rp 7.100.000</b></div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">
                                                        Nilai yang harus ditransfer
                                                    </div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt text-success"><b>Rp 13.500.000</b></span>
                                                    </div>
                                                
                                                    <div class="col-md-9 pt-3 pb-3 tb-title">Pilihan Bank Transfer</div>
                                                    
                                                    <div class="col-md-3 pt-3 pb-3 tb-cnt">BCA</div>
                                                
                                                    <div class="col-md-7 pt-3 pb-3 tb-title">Bukti Bayar</div>
                                                    
                                                    <div class="col-md-5 pt-3 pb-3 tb-cnt">
                                                        <a href="" data-toggle="modal" data-target=".img-tagihan"><img src="assets/images/struk.jpg" alt=""></a>
                                                    </div>
                                            </div>
                                               

                                            <div class="alert alert-warning">
                                               Proses verifikasi memerlukan waktu 1x24jam
                                            </div>

                                            <div class="text-center">
                                                    <a href="" data-toggle="modal" data-target=".konfirm-tagihan" class="btn btn-primary">Konfirmasi</a>
                                                    <a class="btn btn-secondary" href="tagihan.php">Kembali</a>
                                                </div>
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>

                    <!-- simpan perubahan -->
                    <div class="modal fade konfirm-tagihan" tabindex="-1" role="dialog" aria-labelledby="daftarRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Konfirmasi</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <div class="text-center">
                                        Pembayaran yang sudah disimpan tidak bisa diubah
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Ya</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- simpan perubahan -->
                    <div class="modal fade img-tagihan" tabindex="-1" role="dialog" aria-labelledby="daftarRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Bukti Pembayaran</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <img class="img-full" src="assets/images/struk.jpg" alt="">
                                    
                                </div>
                                <div class="modal-footer text-center">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                </div>
                            </div>
                        </div>
                    </div>
