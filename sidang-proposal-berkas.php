<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Sidang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Proposal Skripsi
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                           <div class="btn-actions-pane-left">
                                                <a class="btn-transition btn btn-primary" href="tambah-berkas.php"><span class="pe-7s-upload"></span> Upload Berkas</a>
                                            </div>
                                            <div class="btn-actions-pane-right">
                                                <a class="btn-transition btn btn-outline-primary" href="sidang-proposal.php"><span class="pe-7s-angle-left"></span> Kembali</a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="align-middle mb-0 table table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th width="180">Tanggal Upload</th>
                                                        <th width="120">Berkas</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>12/06/2020</td>
                                                            <td>
                                                                <span class="mb-2 mr-2 btn btn-info"><span class="pe-7s-folder"></span> Berkas</span>
                                                            </td>
                                                            <td>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa placeat, tempore maiores labore odio minima repellendus et unde, libero quae, possimus numquam enim consequatur iste officia temporibus laborum amet ex.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>12/06/2020</td>
                                                            <td>
                                                                <span class="mb-2 mr-2 btn btn-info"><span class="pe-7s-folder"></span> Berkas</span>
                                                            </td>
                                                            <td>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa placeat, tempore maiores labore odio minima repellendus et unde, libero quae, possimus numquam enim consequatur iste officia temporibus laborum amet ex.
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>

                                
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                    