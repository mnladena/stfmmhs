<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Sidang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Sidang Komprehensif
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                                <tr>
                                                    <td>Jadwal Pengajuan Sidang Komprehensif/UKK</td>
                                                    <td>:</td>
                                                    <td>01/06/2020 - 05/06/2020</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <div class="btn-actions">
                                                <a class="btn-transition btn btn-outline-primary" href="sidang-komprehensif-pengajuan.php"><i class="pe-7s-notebook"></i> Pengajuan Sidang Komprehensif/KTI</a>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="mb-0 table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Mahasiswa</th>
                                                    <th>Tanggal Sidang Komprehensif/UKK</th>
                                                    <th>Status</th>
                                                    <th>Nilai</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div>NIM: 110873939</div>
                                                            <div class="mt-2">
                                                               Nama: Salahuddin Yusuf
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <div>Hari/Tanggal: Senin, 21/10/2019</div>
                                                            <div class="mt-2">Waktu: 08.30 - 09.30</div>
                                                        </td>
                                                        <td>
                                                            <div class="mt-2">
                                                                <span class="text-danger">Belum Diverifikasi</span>
                                                                <span class="text-success">Sudah Diverifikasi</span>
                                                            </div>
                                                        </td>
                                                        <td width="200">
                                                            A
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>

                                
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                     <!-- modal -->

                    
