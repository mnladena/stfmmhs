<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Sidang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Skripsi/KTI
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                                <tr>
                                                    <td>Jadwal Pengajuan Sidang Skripsi/KTI</td>
                                                    <td>:</td>
                                                    <td>01/06/2020 - 05/06/2020</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <div class="btn-actions">
                                                <a class="btn-transition btn btn-outline-primary" href="sidang-skripsi-pengajuan.php"><i class="pe-7s-notebook"></i> Pengajuan Sidang Skripsi/KTI</a>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="mb-0 table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Skripsi/KTI</th>
                                                    <th>Jadwal Sidang</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div>Judul: Pembuatan Obat Asam Urat dengan Metode Tradisional Kalimantan Timur</div>
                                                            <div class="mt-2">
                                                                Pembimbing:
                                                                <ol>
                                                                    <li>Dr. Syamsul Rizal</li>
                                                                    <li>Aliando Syarief</li>
                                                                </ol>
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <div>Hari/Tanggal: Senin, 21/10/2019</div>
                                                            <div class="mt-2">Waktu: 08.30 - 09.30</div>
                                                        </td>
                                                        <td>
                                                            <div class="mt-2">
                                                                Pengajuan Sidang Skripsi: <span class="text-danger">Belum Diverifikasi</span>
                                                                <span class="text-success">Valid</span>
                                                            </div>
                                                        </td>
                                                        <td width="200">
                                                            <a class="btn-transition btn btn-outline-info mb-2" href="sidang-skripsi-berkas.php"><i class="pe-7s-folder"></i> Konsep Proposal</a>
                                                            <a class="btn-transition btn btn-outline-info mb-2" href="sidang-skripsi-bimbingan.php"><i class="pe-7s-users"></i> Bimbingan</a>
                                                            <a class="btn-transition btn btn-outline-info mb-2" href="info-berkas.php"><i class="pe-7s-notebook"></i> Pengajuan Skripsi/KTI</a>
                                                            <a class="btn-transition btn btn-outline-info mb-2" href="sidang-skripsi-nilai.php"><i class="pe-7s-notebook"></i> Hasil Penilaian Sidang</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>

                                
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                     <!-- modal -->

                    <!-- daftar remedial -->
                    <div class="modal fade daftar-remedial" tabindex="-1" role="dialog" aria-labelledby="daftarRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <table class="align-middle mb-0 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                                <th>Pilih</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                                <td>
                                                    <div class="custom-checkbox custom-control">
                                                        <input type="checkbox" id="check1" class="custom-control-input">
                                                        <label class="custom-control-label" for="check1"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                                <td>
                                                    <div class="custom-checkbox custom-control">
                                                        <input type="checkbox" id="check2" class="custom-control-input">
                                                        <label class="custom-control-label" for="check2"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="2">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div class="mt-3 text-muted">Catatan: Mata kuliah yang dipilih  untuk remedial harus mendapatkan persetujuan dari Ketua Prodi dan Dosen Pembimbing Akademik</div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- info remedial ditolak -->
                    <div class="modal fade remedial-tolak" tabindex="-1" role="dialog" aria-labelledby="tolakRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Informasi Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <div class="alert alert-danger text-center"><h4>DITOLAK</h4></div>

                                    <table class="align-middle mb-2 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="1">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div>
                                        <b>Catatan Ketua Prodi (Dr. Miftah Farid)</b>
                                        <div class="text-muted"><span class="text-danger">Ditolak</span> kamu tidak perlu mengulang mata kuliah ini</div>
                                    </div>
                                    <div class="divider"></div>
                                    <div>
                                        <b>Catatan Dosen Pembimbing Akademi (Dr. Anwar Fuadi)</b>
                                        <div class="text-muted"><span class="text-success">Diterima</span> silakan lanjutkan</div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- info remedial tunggu -->
                    <div class="modal fade remedial-tunggu" tabindex="-1" role="dialog" aria-labelledby="tolakRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Informasi Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <div class="alert alert-secondary text-center"><h4>MENUNGGU</h4></div>

                                    <table class="align-middle mb-2 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="1">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div>
                                        <b>Catatan Ketua Prodi (Dr. Miftah Farid)</b>
                                        <div class="text-muted"><span class="text-danger">Ditolak</span> kamu tidak perlu mengulang mata kuliah ini</div>
                                    </div>
                                    <div class="divider"></div>
                                    <div>
                                        <b>Catatan Dosen Pembimbing Akademi (Dr. Anwar Fuadi)</b>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- info remedial diterima -->
                    <div class="modal fade remedial-sukses" tabindex="-1" role="dialog" aria-labelledby="tolakRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Informasi Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <div class="alert alert-success text-center"><h4>DITERIMA</h4></div>

                                    <table class="align-middle mb-2 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="1">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div>
                                        <b>Catatan Ketua Prodi (Dr. Miftah Farid)</b>
                                        <div class="text-muted"><span class="text-success">Diterima</span> sudah sesuai, silahkan lanjutkan</div>
                                    </div>
                                    <div class="divider"></div>
                                    <div>
                                        <b>Catatan Dosen Pembimbing Akademi (Dr. Anwar Fuadi)</b>
                                        <div class="text-muted"><span class="text-success">Diterima</span> silakan lanjutkan</div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
