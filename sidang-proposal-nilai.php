<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Sidang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Proposal Skripsi
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Ujian</td>
                                                    <td>:</td>
                                                    <td>07/07/2020</td>
                                                </tr>
                                                <tr>
                                                    <td>Judul Seminar</td>
                                                    <td>:</td>
                                                    <td>Pembuatan Obat Herbal Tradisional</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <ul class="nav">
                                                <li class="nav-item"><a data-toggle="tab" href="#nilai" class="active nav-link">Nilai Hasil Sidang</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#perbaikan" class="nav-link">Perbaikan</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#berita" class="nav-link">Berita Acara</a></li>
                                            </ul>
                                            <div class="btn-actions-pane-right">
                                                <a class="btn-transition btn btn-outline-primary"  href="sidang-proposal.php"><span class="pe-7s-angle-left"></span> Kembali</a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="nilai" role="tabpanel">
                                                    <div class="main-card mb-3 card card-border">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table class="align-middle mb-0 table table-striped table-hover">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Pembimbing 1</td>
                                                                            <td width="100">86</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Pembimbing 2</td>
                                                                            <td width="100">90</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Penguji 1</td>
                                                                            <td width="100">86</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Penguji 2</td>
                                                                            <td width="100">80</td>
                                                                        </tr>
                                                                        <tr class="total">
                                                                            <td>Total Nilai Keseluruhan</td>
                                                                            <td width="100">80</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nilai Rata-rata</td>
                                                                            <td width="100">81.25</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nilai Akhir</td>
                                                                            <td width="100">A</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Dinyatakan</td>
                                                                            <td width="100"><b>LULUS</b></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="perbaikan" role="tabpanel">
                                                    <div class="main-card mb-3 card card-border">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>Bab/Halaman</th>
                                                                        <th>Perbaikan yang Dianjurkan</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>BAB I</td>
                                                                            <td>
                                                                                <div class="mb-3">
                                                                                    <b>Penguji 1:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Penguji 2:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Pembimbing 1:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Pembimbing 2:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>BAB II</td>
                                                                            <td>
                                                                                <div class="mb-3">
                                                                                    <b>Penguji 1:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Penguji 2:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Pembimbing 1:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Pembimbing 2:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>BAB III</td>
                                                                            <td>
                                                                                <div class="mb-3">
                                                                                    <b>Penguji 1:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Penguji 2:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Pembimbing 1:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                                <div class="mb-3">
                                                                                    <b>Pembimbing 2:</b> 
                                                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores explicabo maxime perspiciatis reprehenderit cumque earum nulla minima dolorem harum dolorum non mollitia repellat dolores nemo, quis commodi saepe dolor, deleniti.
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div class="mt-3">
                                                                <b>Perbaikan Judul Skripsi:</b> - 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane active" id="berita" role="tabpanel">
                                                    <div class="main-card mb-3 card card-border">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table class="align-middle mb-0 table table-striped table-hover">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Pembimbing 1</td>
                                                                            <td width="100">86</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Pembimbing 2</td>
                                                                            <td width="100">90</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Penguji 1</td>
                                                                            <td width="100">86</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Total Nilai Dosen Penguji 2</td>
                                                                            <td width="100">80</td>
                                                                        </tr>
                                                                        <tr class="total">
                                                                            <td>Total Nilai Keseluruhan</td>
                                                                            <td width="100">80</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nilai Rata-rata</td>
                                                                            <td width="100">81.25</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nilai Akhir</td>
                                                                            <td width="100">A</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Dinyatakan</td>
                                                                            <td width="100"><b>LULUS</b></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>

                                
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                    