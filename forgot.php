<?php include "include/head.php" ?>


<div class="app-main">
    <div class="app-main__inner">

<div class="log-content">
    
    <header class="log-header">
        <img class="logo" src="assets/images/logo.png" alt="">
    </header>

    
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-xl-6">
            <div class="main-card mb-3 card card-border">
                <div class="card-body">
                    <h5 class="card-title">Hei Mahasiswa hebat, Kamu lupa password?</h5>
                    <p>Jangan khawatir, kamu bisa reset password kamu dengan memasukkan emailmu yang terdaftar pada Sistem Akademik Sekolah Tinggi farmasi Muhammadiyah Tangerang</p>
                    <form class="log-form">
                        <div class="position-relative form-group"><label for="exampleEmail" class="">Email</label><input name="email" id="exampleEmail" placeholder="masukkan email" type="email" class="form-control"></div>
                        <div id="html_element"></div>
                        <button class="mt-1 btn btn-success btn-block">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

</div>
</div>
<!-- footer khusus halaman login/daftar -->
<footer class="footer-log">
    Sekolah Tinggi Farmasi Muhammadiyah Tangerang
</footer>
<!-- footer khusus halaman login/daftar -->
