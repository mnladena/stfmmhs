<?php include "include/head.php" ?>


<div class="app-main">
    <div class="app-main__inner">

<div class="log-content">
    
    <header class="log-header">
        <img class="logo" src="assets/images/logo.png" alt="">
    </header>

    
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-xl-6">
            <div class="main-card mb-3 card card-border">
                <div class="card-body">
                    <h5 class="card-title">Pemulihan Password</h5>

                    <form class="log-form">
                        
                        <div class="position-relative form-group"><label for="examplePassword" class="">Password</label><input name="password" id="password" placeholder="password baru" type="password" class="form-control"></div>
                        <div class="position-relative form-group"><label for="examplePassword" class="">Konfirmasi Password</label><input name="password" id="confirmpassword" placeholder="ketik ulang password baru" type="password" class="form-control"></div>
                        <div id="html_element"></div>
                        <button class="mt-1 btn btn-success btn-block">Kirim</button>
                        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
                        </script>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

</div>
</div>
<!-- footer khusus halaman login/daftar -->
<footer class="footer-log">
    Sekolah Tinggi Farmasi Muhammadiyah Tangerang
</footer>
<!-- footer khusus halaman login/daftar -->
