<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="breadcrumb-item"><a href="">Pengujian Sidang</a></li>
                            <li class="active breadcrumb-item" aria-current="page">AIK</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Pengujian Sidang
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            AIK
                                        </div>
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1-Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1 Farmasi</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-header">
                                            Kategori Materi : Al-Islam
                                        </div>
                                        <div class="card-body">

                                                    <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                        <thead>
                                                           <tr>
                                                                <th>No</th>
                                                                <th>Penilaian</th>
                                                                <th width="200">Nilai Angka</th>
                                                                <th width="150">Nilai Huruf</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td width="30">1</td>
                                                                <td>
                                                                    Baca Al Qur'an dan Tajwid
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                                <td>
                                                                   
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">2</td>
                                                                <td>
                                                                    Hafalan Surat-surat Pendek
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                                <td>
                                                                   
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">3</td>
                                                                <td>
                                                                    Doa-doa Harian
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                                <td>
                                                                   
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2">Total Angka</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">Rata-rata</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">Nilai Huruf</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div class="alert alert-info">
                                                        Keterangan: A=80-100, B=69-79, C=56-67, D=<50
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-md-2">
                                                           <b> Catatan </b>
                                                        </label>
                                                        <div class="col-md-10">
                                                           <textarea name="text" id="exampleText" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="text-center mt-3">
                                                        <a href="" data-toggle="modal" data-target=".tambah-bimbingan" class="btn btn-primary">Simpan</a>
                                                        <button class="btn btn-secondary" type="reset">Batal</button>
                                                    </div>
                                                </div>

                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>


                    <!-- simpan perubahan -->
                    <div class="modal fade tambah-bimbingan" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Konfirmasi</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <div class="text-center">
                                        Apakah Anda yakin ingin menyimpan penilaian?
                                        <br>
                                        penilaian tidak bisa diubah kembali setelah disimpan
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    
