<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="breadcrumb-item"><a href="">Pengujian Sidang</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Proposal</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Pengujian Sidang
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Proposal Skripsi/KTI
                                        </div>
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td>
                                                    <td width="50">:</td>
                                                    <td>S1-Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1 Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Judul Seminar</td>
                                                    <td>:</td>
                                                    <td>Pembuatan Obat Asam Urat dengan Metode Herbal</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab1" class="active nav-link">Perbaikan Seinar Proposal</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab2" class="nav-link">Penilaian Seminar Proposal</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                
                                                <!-- Pembimbing 1 -->
                                                <div class="tab-pane active" id="tab1" role="tabpanel">
                                                    
                                                    <div class="card-header">
                                                        Perbaikan Seminar Proposal
                                                    </div>
                                                    <div class="card-body">

                                                        <div class="row">
                                                            <label class="col-md-2">Bab 1 :</label>
                                                            <div class="col-md-8 mb-3">
                                                                <textarea class="form-control" name="" id=""></textarea>
                                                                <div class="alert alert-success">Data berhasil disimpan!</div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn-transition btn btn-primary"><i class="pe-7s-diskette"></i> Simpan</button>
                                                            </div>

                                                            <label class="col-md-2">Bab 2 :</label>
                                                            <div class="col-md-8 mb-3">
                                                                <textarea class="form-control" name="" id=""></textarea>
                                                                <div class="alert alert-danger">Data gagal disimpan!</div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn-transition btn btn-primary"><i class="pe-7s-diskette"></i> Simpan</button>
                                                            </div>

                                                            <label class="col-md-2">Bab 3 :</label>
                                                            <div class="col-md-8 mb-3">
                                                                <textarea class="form-control" name="" id=""></textarea>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn-transition btn btn-primary"><i class="pe-7s-diskette"></i> Simpan</button>
                                                            </div>

                                                            <label class="col-md-2">Bab 4 :</label>
                                                            <div class="col-md-8 mb-3">
                                                                <textarea class="form-control" name="" id=""></textarea>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn-transition btn btn-primary"><i class="pe-7s-diskette"></i> Simpan</button>
                                                            </div>

                                                            <label class="col-md-2">Bab 5 :</label>
                                                            <div class="col-md-8 mb-3">
                                                                <textarea class="form-control" name="" id=""></textarea>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn-transition btn btn-primary"><i class="pe-7s-diskette"></i> Simpan</button>
                                                            </div>

                                                            <label class="col-md-2">Perbaikan Judul Skripsi :</label>
                                                            <div class="col-md-8 mb-3">
                                                                <textarea class="form-control" name="" id=""></textarea>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="btn-transition btn btn-primary"><i class="pe-7s-diskette"></i> Simpan</button>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>

                                                </div>
                                                <!-- Pembimbing 1 -->

                                                <!-- Pembimbing 2 -->
                                                <div class="tab-pane" id="tab2" role="tabpanel">

                                                   <div class="card-header">
                                                        Penilaian Seminar Proposal
                                                        
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                        <thead>
                                                           <tr>
                                                                <th>No</th>
                                                                <th>Kriteria</th>
                                                                <th>Penilaian</th>
                                                                <th width="200">Nilai Angka</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td width="30">1</td>
                                                                <td class="text-center" rowspan="4">
                                                                    Teori
                                                                </td>
                                                                <td>
                                                                    Penyajian
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">2</td>
                                                                <td>
                                                                    Isi Paper
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">3</td>
                                                                <td>
                                                                    Penguasaan materi/isi/metode penilaian
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">4</td>
                                                                <td>
                                                                    Pengetahuan Umum
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">1</td>
                                                                <td rowspan="2" class="text-center">Kepribadian</td>
                                                                <td>
                                                                    Kerapian
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">2</td>
                                                                <td>
                                                                    Sikap
                                                                </td>
                                                                <td>
                                                                    <input type="text" name="" value="79">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="3">Total Angka</td>
                                                                <td>474</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">Rata-rata</td>
                                                                <td>79</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">Nilai Huruf</td>
                                                                <td>B</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div class="alert alert-info">
                                                        Keterangan: A=80-100, B=69-79, C=56-67, D=<50
                                                    </div>
                                                    <div class="row">
                                                        <label class="col-md-2">
                                                           <b> Catatan </b>
                                                        </label>
                                                        <div class="col-md-10">
                                                           <textarea name="text" id="exampleText" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="text-center mt-3">
                                                        <a href="" data-toggle="modal" data-target=".tambah-bimbingan" class="btn btn-primary">Simpan</a>
                                                        <button class="btn btn-secondary" type="reset">Batal</button>
                                                    </div>
                                                        
                                                    </div>

                                                </div>
                                                <!-- Pembimbing 2 -->

                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>

                    <!-- simpan perubahan -->
                    <div class="modal fade tambah-bimbingan" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Konfirmasi</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <div class="text-center">
                                        Apakah Anda yakin ingin menyimpan penilaian?
                                        <br>
                                        penilaian tidak bisa diubah kembali setelah disimpan
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
