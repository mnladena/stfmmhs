<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Kelas</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Jadwal Mengajar Tahun Ajaran 2019/2020 Semester 6 - Genap
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Jadwal Mengajar Teori
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Kode MK</th>
                                                    <th>Mata Kuliah</th>
                                                    <th>SKS</th>
                                                    <th>Kelas</th>
                                                    <th>Hari</th>
                                                    <th>Jam</th>
                                                    <th>Ruangan</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row">MPK-1202</th>
                                                    <td>AIK 1 (Kemanusiaan dan keimanan)</td>
                                                    <td>2(0)</td>
                                                    <td>01FSA</td>
                                                    <td>Senin</td>
                                                    <td>09.00 - 09.30</td>
                                                    <td>L204</td>
                                                    <td>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="absensi.php"><i class="pe-7s-note2"></i> Absensi</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="penilaian.php"><i class="pe-7s-pen"></i> Penilaian</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-kuliah.php"><i class="pe-7s-note"></i> Materi</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">MPK-1202</th>
                                                    <td>Sidang AIK</td>
                                                    <td>3(0)</td>
                                                    <td>01FSA</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">MPK-1202</th>
                                                    <td>AIK 1 (Kemanusiaan dan keimanan)</td>
                                                    <td>2(0)</td>
                                                    <td>01FSA</td>
                                                    <td>Senin</td>
                                                    <td>09.00 - 09.30</td>
                                                    <td>L204</td>
                                                    <td>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="absensi.php"><i class="pe-7s-note2"></i> Absensi</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="penilaian.php"><i class="pe-7s-pen"></i> Penilaian</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-kuliah.php"><i class="pe-7s-note"></i> Materi</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">MPK-1202</th>
                                                    <td>AIK 1 (Kemanusiaan dan keimanan)</td>
                                                    <td>2(0)</td>
                                                    <td>01FSA</td>
                                                    <td>Senin</td>
                                                    <td>09.00 - 09.30</td>
                                                    <td>L204</td>
                                                    <td>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="absensi.php"><i class="pe-7s-note2"></i> Absensi</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="penilaian.php"><i class="pe-7s-pen"></i> Penilaian</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-kuliah.php"><i class="pe-7s-note"></i> Materi</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">MPK-1202</th>
                                                    <td>AIK 1 (Kemanusiaan dan keimanan)</td>
                                                    <td>2(0)</td>
                                                    <td>01FSA</td>
                                                    <td>Senin</td>
                                                    <td>09.00 - 09.30</td>
                                                    <td>L204</td>
                                                    <td>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="absensi.php"><i class="pe-7s-note2"></i> Absensi</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="penilaian.php"><i class="pe-7s-pen"></i> Penilaian</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-kuliah.php"><i class="pe-7s-note"></i> Materi</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">MPK-1202</th>
                                                    <td>AIK 1 (Kemanusiaan dan keimanan)</td>
                                                    <td>2(0)</td>
                                                    <td>01FSA</td>
                                                    <td>Senin</td>
                                                    <td>09.00 - 09.30</td>
                                                    <td>L204</td>
                                                    <td>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="absensi.php"><i class="pe-7s-note2"></i> Absensi</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="penilaian.php"><i class="pe-7s-pen"></i> Penilaian</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-kuliah.php"><i class="pe-7s-note"></i> Materi</a>
                                                    </td>
                                                </tr>
                                                
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                        <div class="card-header">
                                            Jadwal Kuliah Praktikum
                                        </div>

                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Kode MK</th>
                                                    <th>Mata Kuliah</th>
                                                    <th>SKS</th>
                                                    <th>Kelas</th>
                                                    <th>Hari</th>
                                                    <th>Jam</th>
                                                    <th>Ruangan</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row">MPK-1202</th>
                                                    <td>AIK 1 (Kemanusiaan dan keimanan)</td>
                                                    <td>2(0)</td>
                                                    <td>01FSA</td>
                                                    <td>Senin</td>
                                                    <td>09.00 - 09.30</td>
                                                    <td>L204</td>
                                                    <td>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="absensi.php"><i class="pe-7s-note2"></i> Absensi</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="penilaian-praktek.php"><i class="pe-7s-pen"></i> Penilaian</a>
                                                        <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-kuliah.php"><i class="pe-7s-note"></i> Materi</a>
                                                    </td>
                                                </tr>
                                                
                                                </tbody>
                                                <tfoot class="total">
                                                    <tr>
                                                         <th colspan="2" scope="row">Total SKS (Teori & Praktikum)</th>
                                                        <td colspan="6">21</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
