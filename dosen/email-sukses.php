<?php include "include/head.php" ?>


<div class="app-main">
    <div class="app-main__inner">

<div class="log-content">
    
    <header class="log-header">
        <img class="logo" src="assets/images/logo.png" alt="">
    </header>

    
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-xl-6">
            <div class="main-card mb-3 card card-border border-success">
                <div class="card-body">
                    <h5 class="card-title">Submit Email</h5>
                    
                    <p>Email kamu berhasil disubmit! silahkan cek email kamu</p>

                    <a href="login.php" class="mb-2 mr-2 btn btn-primary"><i class="pe-7s-back"></i> Kembali ke halaman login</a>
                    
                </div>
            </div>
        </div>
    </div>

</div>

</div>
</div>
<!-- footer khusus halaman login/daftar -->
<footer class="footer-log">
    Sekolah Tinggi Farmasi Muhammadiyah Tangerang
</footer>
<!-- footer khusus halaman login/daftar -->
