<?php include "include/head.php" ?>

<script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : 'your_site_key'
        });
      };
    </script>

<div class="app-main">
    <div class="app-main__inner">

<div class="log-content">
    
    <header class="log-header">
        <img class="logo" src="assets/images/logo.png" alt="">
    </header>

    <div class="row">

        <div class="col-md-8 col-xl-8 order-lg-0 order-1">
           <div class="main-card mb-3 card card-border">
                <div class="card-header full">
                    <div class="row">
                        <div class="col-md-7 col-xl-7">
                            <div class="card-header__text">Pengumuman</div>
                        </div>

                        <div class="col-md-5 col-xl-5 inline">
                            <label>Arsip</label>
                            <div class="position-relative form-group">
                                <select type="select" id="exampleCustomSelect" name="customSelect" class="custom-select">
                                    <option value="">Bulan</option>
                                    <option>Januari</option>
                                    <option>Februari</option>
                                    <option>Maret</option>
                                    <option>April</option>
                                    <option>Mei</option>
                                    <option>Juni</option>
                                    <option>Juli</option>
                                    <option>Agustus</option>
                                    <option>September</option>
                                    <option>Oktober</option>
                                    <option>November</option>
                                    <option>Desember</option>
                                </select>
                            </div>
                        
                            <div class="position-relative form-group ml-5">
                                <select type="select" id="exampleCustomSelect" name="customSelect" class="custom-select">
                                    <option value="">Tahun</option>
                                    <option>2015</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option>2018</option>
                                    <option>2019</option>
                                    <option>2020</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-body title-text">

                    <div id="accordion" class="accordion-wrapper mb-3">
                        <div class="card">
                            <div id="headingOne" class="card-header">
                                <button type="button" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne" class="text-left m-0 p-0 btn btn-link btn-block btn-accord">
                                    <div class="title-news">
                                        <span class="title-text__date">21 Maret 2019</span>
                                        <h5 class="m-0 p-0">Pengumuman Perbaikan Gedung F-30</h5>
                                    </div>
                                    <i class="pe-7s-angle-up-circle"></i>
                                </button>
                            </div>
                            <div data-parent="#accordion" id="collapseOne1" aria-labelledby="headingOne" class="collapse">
                                <div class="card-body">
                                    <div>1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                    nesciunt
                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                    VHS.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div id="headingTwo" class="b-radius-0 card-header">
                                <button type="button" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="false" aria-controls="collapseTwo" class="text-left m-0 p-0 btn btn-link btn-block btn-accord">
                                    <div class="title-news">
                                        <span class="title-text__date">21 Maret 2019</span>
                                        <h5 class="m-0 p-0">Hasil Kelulusan Telah Keluar</h5>
                                    </div>
                                    <i class="pe-7s-angle-up-circle"></i></button>
                            </div>
                            <div data-parent="#accordion" id="collapseOne2" class="collapse">
                                <div class="card-body">2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                    nesciunt
                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                    VHS.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div id="headingThree" class="card-header">
                                <button type="button" data-toggle="collapse" data-target="#collapseOne3" aria-expanded="false" aria-controls="collapseThree" class="text-left m-0 p-0 btn btn-link btn-block btn-accord">
                                    <div class="title-news">
                                        <span class="title-text__date">21 Maret 2019</span>
                                        <h5 class="m-0 p-0">Pengumuan Libur Bersama Sesuai Ketetapan Pemerintah</h5>
                                    </div>
                                    <i class="pe-7s-angle-up-circle"></i></button>
                            </div>
                            <div data-parent="#accordion" id="collapseOne3" class="collapse">
                                <div class="card-body">3. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                    nesciunt
                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                    VHS.
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <a href="berita.php">
                        <span class="title-text__date">21 Maret 2019</span>
                        <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                    </a>
                    <a href="berita.php">
                        <span class="title-text__date">22 Maret 2019</span>
                        <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                    </a>
                    <a href="berita.php">
                        <span class="title-text__date">21 Maret 2019</span>
                        <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                    </a>
                    <a href="berita.php">
                        <span class="title-text__date">22 Maret 2019</span>
                        <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                    </a>
                    <a href="berita.php">
                        <span class="title-text__date">21 Maret 2019</span>
                        <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                    </a> -->
                </div>
                <nav class="" aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                            <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                            <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                        </ul>
                    </nav>
            </div>
        </div>

        <div class="col-md-4 col-xl-4 order-lg-1 order-0">
            <div class="main-card mb-3 card text-white bg-primary">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <form class="log-form">
                        <div class="position-relative form-group"><label for="exampleEmail" class="">Email</label><input name="email" id="exampleEmail" placeholder="email" type="email" class="form-control"></div>
                        <div class="position-relative form-group"><label for="examplePassword" class="">Password</label><input name="password" id="examplePassword" placeholder="passwword" type="password" class="form-control"></div>
                        <div id="html_element"></div>
                    
                </div>
                <div class="card-footer">
                    <button class="mt-1 btn btn-warning btn-block">Submit</button>
                    <a href="forgot.php" class="mb-2 mr-2 border-0 btn-transition btn btn-outline-link btn-block  text-white">Lupa Password?</a>
                        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer>
                        </script>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

</div>
</div>
<!-- footer khusus halaman login/daftar -->
<footer class="footer-log">
    Sekolah Tinggi Farmasi Muhammadiyah Tangerang
</footer>
<!-- footer khusus halaman login/daftar -->

<script type="text/javascript" src="./assets/scripts/main.js"></script></body>
