<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Menu Utama</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Halaman Utama</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Profil
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab1" class="active nav-link">Profil Pribadi</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                
                                                <!-- profil pribadi -->
                                                <div class="tab-pane active" id="tab1" role="tabpanel">
                                                    
                                                    <table class="mb-0 table table-data table-striped">

                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>
                                                                <div class="profile-img">
                                                                    <img src="assets/images/user.png" alt="">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>NIDN</td>
                                                            <td>201908721321</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama</td>
                                                            <td>Andrew Maryono</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jenis Kelamin</td>
                                                            <td>Laki-laki</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tempat tanggal lahir</td>
                                                            <td>Depok - 12 Desember 2013</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Agama</td>
                                                            <td>Islam</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alamat KTP</td>
                                                            <td>Jl. In Ajadulu no 45 RT 01/RW 03. Manokwari, Papua Barat</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Alamat Tempat Tinggal</td>
                                                            <td>Jl. Jalan yuk no 2 RT 01/RW 03. Mampang, Jakarta Selatan</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kewarganegaraan</td>
                                                            <td>Indonesia</td>
                                                        </tr>
                                                        <tr>
                                                            <td>No HP</td>
                                                            <td>08122987679</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>botak.bersinar@gmail.com</td>
                                                        </tr>

                                                    </table>

                                                </div>
                                                <!-- profil pribadi -->


                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
