<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Bimbingan Skripsi
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <div class="btn-actions-pane-right">
                                                <a href="kelas.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>Judul Proposal</td>
                                                    <td>:</td>
                                                    <td>Pembuatan Obat Asam Urat dengan Metode Herbal</td>
                                                </tr>
                                                <tr>
                                                    <td>Judul Skripsi/KTI</td>
                                                    <td>:</td>
                                                    <td>Pembuatan Obat Asam Urat dengan Metode Herbal</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                                
                                                    
                                                    <div class="card-header">
                                                        Dosen Pembimbing: Dr. Syamsul Chaerudin
                                                        <div class="btn-actions-pane-right">
                                                            <a href="tambah-bimbingan-skripsi.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-plus"></i> Tambah Bimbingan</a>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                               <tr>
                                                                    <th>No</th>
                                                                    <th width="180">Tanggal Bimbingan</th>
                                                                    <th>Catatan Bimbingan</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>
                                                                        12/06/2020
                                                                    </td>
                                                                    <td>
                                                                       Sudah bagus, hanya saja penulisannya masih tidak baku. Perlu disusun ulang lagi kalau perlu semuanya dirombak saja. Semangat!
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>
                                                                        14/06/2020
                                                                    </td>
                                                                    <td>
                                                                        Pada Bab I, alangkah baiknya disertakan nama saya, karena saya yang membimbing. Jangan Lupakan jasa-jasa saya. OKE!
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>
                                        </div>
                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>

                    
