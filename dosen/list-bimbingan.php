<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Bimbingan Akademik</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Bimbingan Akademik
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Bimbingan Akademik
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Mahasiswa</th>
                                                    <th>Jumlah Bimbingan</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>02031987 - Dodi Cahyadi</td>
                                                        <td>
                                                           2
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="isi-bimbingan.php"><i class="pe-7s-note2"></i> Isi Bimbingan</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="khs-bimbingan.php"><i class="pe-7s-pen"></i> KHS</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="catatan-bimbingan.php"><i class="pe-7s-note"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>02031987 - Dodi Cahyadi</td>
                                                        <td>
                                                           2
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="isi-bimbingan.php"><i class="pe-7s-note2"></i> Isi Bimbingan</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="khs-bimbingan.php"><i class="pe-7s-pen"></i> KHS</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href=" catatan-bimbingan.php"><i class="pe-7s-note"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>02031987 - Dodi Cahyadi</td>
                                                        <td>
                                                           2
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="isi-bimbingan.php"><i class="pe-7s-note2"></i> Isi Bimbingan</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="khs-bimbingan.php"><i class="pe-7s-pen"></i> KHS</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="catatan-bimbingan.php"><i class="pe-7s-note"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>02031987 - Dodi Cahyadi</td>
                                                        <td>
                                                           2
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="isi-bimbingan.php"><i class="pe-7s-note2"></i> Isi Bimbingan</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="khs-bimbingan.php"><i class="pe-7s-pen"></i> KHS</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="catatan-bimbingan.php"><i class="pe-7s-note"></i> Detail</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
