<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Menu Utama</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Halaman Utama</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Halaman Utama
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-7 col-xl-7">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Berita/Pengumuman</div>
                                         <div class="card-body title-text">
                                            <a href="berita.php">
                                                <span class="title-text__date">21 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">22 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">21 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">22 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>
                                            </a>
                                            <a href="berita.php">
                                                <span class="title-text__date">21 Maret 2019</span>
                                                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod amet voluptate laborum at itaque distinctio ullam.</h2>
                                            </a>


                                         <nav class="mt-3" aria-label="page navigation">
                                                <ul class="pagination">
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Previous"><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                                                    <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                                                    <li class="page-item"><a href="javascript:void(0);" class="page-link" aria-label="Next"><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5 col-xl-5">
                                    <div class="main-card mb-3 card">
                                        <div class="card-header">Perkuliahan Saat Ini</div>
                                        <div class="card-body">
                                            <?php if(isset($_GET['kosong'])){ ?>
                                                <div class="alert alert-info text-center" role="alert">
                                                    <p>Saat ini tidak ada perkuliahan</p>
                                                </div>
                                            <?php } else {?>

                                                <h5>Perkembangan Farmasi</h5>
                                                <table class="mb-0 table table-borderless">
                                                    <tr>
                                                        <td>Jam</td>
                                                        <td>:</td>
                                                        <td>09.00 - 10.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ruang</td>
                                                        <td>:</td>
                                                        <td>H.102</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Pertemuan</td>
                                                        <td>:</td>
                                                        <td>2</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kelas</td>
                                                        <td>:</td>
                                                        <td>14A</td>
                                                    </tr>
                                                </table> 
                                                <div class="text-center mt-3">
                                                    <button class="btn btn-primary btn-block" data-toggle="modal" data-target=".checkin-mhs">Checkin Mahasiswa</button>
                                                    <button class="btn btn-danger btn-block" data-toggle="modal" data-target=".selesai-kuliah">Selesai Perkuliahan</button>
                                                </div>
                                           
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>


                    <!-- checkin mahasiswa -->
                    <div class="modal fade checkin-mhs" tabindex="-1" role="dialog" aria-labelledby="checkinMhs" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Absensi Mahasiswa</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <div class="row">
                                      <div class="col-sm-6 col-md-6">

                                        <?php if(isset($_GET['habis'])){ ?>

                                            <div class="alert alert-warning text-center" role="alert">
                                                <h4> 00:00</h4>
                                                <p class="mb-0">Batas waktu scan QR Code</p>
                                            </div>
                                            <div class="alert alert-danger text-center mt-5">
                                                Waktu absensi sudah habis
                                            </div>
                                        
                                        <?php } else { ?>
                                            <div class="alert alert-warning text-center" role="alert">
                                                <h4> 08:34</h4>
                                                <p class="mb-0">Batas waktu scan QR Code</p>
                                            </div>
                                            <div class="text-center qrimg">
                                              <img src="assets/images/qr.png">
                                            </div>
                                            <div class="text-center">Silahkan Scan QR Code di atas</div>
                                        <?php } ?>

                                      </div>

                                      <div class="col-sm-6 col-md-6">
                                        <div class="info-list">
                                          <div class="info-label">Kode MK</div>
                                          <div class="info-data">MPK-12101</div>

                                          <div class="info-label">Nama MK</div>
                                          <div class="info-data">AIK 1 (Kemanusiaan dan Keimanan)</div>

                                          <div class="info-label">SKS</div>
                                          <div class="info-data">2/0</div>

                                          <div class="info-label">Kelas</div>
                                          <div class="info-data">14A</div>

                                          <div class="info-label">Pertemuan</div>
                                          <div class="info-data">Pertemuan ke-3</div>

                                          <div class="info-label">Dosen</div>
                                          <div class="info-data">Muhammad Sanusi Lc</div>

                                          <div class="info-label">Waktu</div>
                                          <div class="info-data">08.00 0 09.00</div>

                                          <button class="btn btn-block btn-success mt-3" data-dismiss="modal">Tutup</button>

                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- checkin mahasiswa -->
                    <div class="modal fade selesai-kuliah" tabindex="-1" role="dialog" aria-labelledby="selesaiKuliah" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Selesai Perkuliahan</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <h4 class="text-center mb-5">Berita Acara Perkuliahan</h4>
                                    
                                    <form class="form-custom needs-validation" novalidate>
                                        <div class="position-relative row form-group">
                                            <label for="exampleEmail" class="col-sm-3 col-form-label">Mata Kuliah</label>
                                            <div class="col-sm-9">
                                                <span class="text-form">
                                                    FTS SOLIDA
                                                </span>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="exampleEmail" class="col-sm-3 col-form-label">Pertemuan</label>
                                            <div class="col-sm-9">
                                                <span class="text-form">
                                                    Pertemuan 1
                                                </span>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="exampleEmail" class="col-sm-3 col-form-label">Hari/Tanggal</label>
                                            <div class="col-sm-9">
                                                <span class="text-form">
                                                    Senin/01-09-2020
                                                </span>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="exampleEmail" class="col-sm-3 col-form-label">Mahasiswa hadir/tidak hadir</label>
                                            <div class="col-sm-9">
                                                <span class="text-form">
                                                    36/34/2
                                                </span>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="exampleEmail" class="col-sm-3 col-form-label">Quiz</label>
                                            <div class="col-sm-9">
                                                <div class="position-relative mt-2 form-check form-check-inline">
                                                    <input name="radio2" type="radio" id="yes" class="form-check-input" required>
                                                    <label class="form-check-label" for="yes"> Ya</label>
                                                    <div class="invalid-feedback invalid-radio">
                                                        Pilih salah satu
                                                    </div>
                                                </div>
                                                <div class="position-relative mt-2 form-check form-check-inline">
                                                    <input name="radio2" type="radio" id="no" class="form-check-input">
                                                    <label class="form-check-label" for="no"> Tidak</label>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="exampleText" class="col-sm-3 col-form-label">Pokok Bahasan</label>
                                            <div class="col-sm-9">
                                                <textarea name="text" id="exampleText" class="form-control" required></textarea>
                                                <div class="invalid-feedback">
                                                    Belum diisi
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="exampleText2" class="col-sm-3 col-form-label">Sub Pokok Bahasan</label>
                                            <div class="col-sm-9">
                                                <textarea name="text" id="exampleText2" class="form-control" required></textarea>
                                                <div class="invalid-feedback">
                                                    Belum diisi
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group"><label for="exampleText3" class="col-sm-3 col-form-label">Kepustakaan</label>
                                            <div class="col-sm-9">
                                                <textarea name="text" id="exampleText3" class="form-control" required></textarea>
                                                <div class="invalid-feedback">
                                                    Belum diisi
                                                </div>
                                            </div>
                                        </div>

                                        <div class="alert alert-warning">Setelah submit, data yang diinput tidak bisa diubah kembali</div>
                                        
                                        <div class="position-relative row form-check mt-3">
                                            <div class="col-sm-12 text-center">
                                                <button class="btn btn-primary" type="submit">Selesai Perkuliahan</button>
                                                <button class="btn btn-secondary" type="reset" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </form>

                                    <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    form.classList.add('was-validated');
                                                }, false);
                                            });
                                        }, false);
                                    })();
                                </script>

                                    
                                </div>
                            </div>
                        </div>
                    </div>





                    
