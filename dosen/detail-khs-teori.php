<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Kartu Hasil Studi</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Kartu Hasil Studi
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun Akademik/Semester</td>
                                                    <td>:</td>
                                                    <td>2015/2016 Ganjil - 1</td>
                                                </tr>
                                                <tr>
                                                    <td>Mata Kuliah</td>
                                                    <td>:</td>
                                                    <td>Pengantar Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nilai Akhir</td>
                                                    <td>:</td>
                                                    <td>84.17</td>
                                                </tr>
                                                <tr>
                                                    <td>Nilai Huruf</td>
                                                    <td>:</td>
                                                    <td>A</td>
                                                </tr>
                                            </table>
                                        </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Akumulasi Nilai
                                            <div class="btn-actions-pane-right">
                                                <button class="btn-transition btn btn-outline-primary"><i class="pe-7s-download"></i> Download Penilaian</button>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">

                                            <div class="row">

                                                <div class="col-md-2 col-xl-2">
                                                    <h5>Range Nilai</h5>
                                                    <div class="table-responsive">
                                                    <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Nilai</th>
                                                            <th>Min</th>
                                                            <th>Max</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>A</td>
                                                                <td>80</td>
                                                                <td>100</td>
                                                            </tr>
                                                            <tr>
                                                                <td>B</td>
                                                                <td>68</td>
                                                                <td>79</td>
                                                            </tr>
                                                            <tr>
                                                                <td>C</td>
                                                                <td>56</td>
                                                                <td>67</td>
                                                            </tr>
                                                            <tr>
                                                                <td>D</td>
                                                                <td>45</td>
                                                                <td>55</td>
                                                            </tr>
                                                            <tr>
                                                                <td>E</td>
                                                                <td>00</td>
                                                                <td>44</td>
                                                            </tr>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                                </div>

                                                <div class="col-md-5 col-xl-5">
                                                    <h5>Nilai Komponen Teori</h5>
                                                    <div class="table-responsive">
                                                    <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Komponen</th>
                                                            <th>Bobot</th>
                                                            <th>Nilai</th>
                                                            <th>Hasil</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Absensi</td>
                                                                <td>10%</td>
                                                                <td>85.7</td>
                                                                <td>85.7</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Tugas & Etika</td>
                                                                <td>30%</td>
                                                                <td>85.7</td>
                                                                <td>85.7</td>
                                                            </tr>
                                                            <tr>
                                                                <td>UTS</td>
                                                                <td>30%</td>
                                                                <td>85.7</td>
                                                                <td>85.7</td>
                                                            </tr>
                                                            <tr>
                                                                <td>UAS</td>
                                                                <td>30%</td>
                                                                <td>85.7</td>
                                                                <td>85.7</td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot class="total">
                                                            <tr>
                                                                <td colspan="3">Total</td>
                                                                <td>85.3</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                                </div>

                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
