<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Absensi Perkuliahan
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Mata Kuliah</td>
                                                    <td>:</td>
                                                    <td>Pengantar Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode MK</td>
                                                    <td>:</td>
                                                    <td>MKB-331203</td>
                                                </tr>
                                                <tr>
                                                    <td>Kelas</td>
                                                    <td>:</td>
                                                    <td>A</td>
                                                </tr>
                                                <tr>
                                                    <td>Waktu Kuliah</td>
                                                    <td>:</td>
                                                    <td>Senin, 08.00 - 09.30</td>
                                                </tr>
                                                <tr>
                                                    <td>Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Salahudin Yusuf</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <div class="btn-actions-pane-right">
                                                <a href="absensi.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Pertemuan</th>
                                                    <th>Tanggal Kuliah</th>
                                                    <th>In</th>
                                                    <th>Out</th>
                                                    <th>Result</th>
                                                    <th>Catatan Dosen</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>-</td>
                                                        <td>-</td>
                                                        <td>Tidak Hadir</td>
                                                        <td>Sakit</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <td>7</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <td>8</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <td>9</td>
                                                        <td>
                                                            <div>21/10/2019</div>
                                                            <div>08.30 - 09.30</div>
                                                        </td>
                                                        <td>08.03</td>
                                                        <td>09.25</td>
                                                        <td>Hadir</td>
                                                        <td>-</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
