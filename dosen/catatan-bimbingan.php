<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Bimbingan Akademik</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Bimbingan Akademik
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Bimbingan</td>
                                                    <td>:</td>
                                                    <td>3</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            
                                            <div class="btn-actions-pane-right">
                                                <div class="position-relative form-group mt-3 ml-5">
                                                    <select type="select" id="exampleCustomSelect" name="customSelect" class="custom-select">
                                                        <option value="">Tahun Akademik</option>
                                                        <option>2015/2016 - Ganjil</option>
                                                        <option>2016/2017 - Genap</option>
                                                        <option>2017/2018 - Ganjil</option>
                                                        <option>2018/2019 - Genap</option>
                                                        <option>2019/2020 - Ganjil</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th width="180">Waktu Bimbingan</th>
                                                    <th>Catatan Bimbingan</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>07/10/2020 08.30</td>
                                                        <td>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias vero tempore, ipsam pariatur ducimus, officia facere sint illum optio hic cum sequi esse autem aliquid. Aut a recusandae perspiciatis vel!
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>07/10/2020 08.30</td>
                                                        <td>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias vero tempore, ipsam pariatur ducimus, officia facere sint illum optio hic cum sequi esse autem aliquid. Aut a recusandae perspiciatis vel!
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>07/10/2020 08.30</td>
                                                        <td>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias vero tempore, ipsam pariatur ducimus, officia facere sint illum optio hic cum sequi esse autem aliquid. Aut a recusandae perspiciatis vel!
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>07/10/2020 08.30</td>
                                                        <td>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias vero tempore, ipsam pariatur ducimus, officia facere sint illum optio hic cum sequi esse autem aliquid. Aut a recusandae perspiciatis vel!
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
