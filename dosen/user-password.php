<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Menu Utama</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Halaman Utama</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Password
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row justify-content-md-center">

                                <div class="col-md-6 col-xl-6">

                                    <div class="alert alert-success fade show" role="alert">Password Berhasil diubah!</div>
                                    <div class="alert alert-danger fade show" role="alert">Password Gagal diubah!</div>

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">

                                            <form class="log-form needs-validation" novalidate>
                                                
                                                <div class="position-relative form-group">
                                                    <label for="examplePassword" class="">Password Lama</label>
                                                    <input name="password" id="password" placeholder="password baru" type="password" class="form-control" required>
                                                    <div class="invalid-feedback">
                                                        Password lama salah
                                                    </div>
                                                </div>

                                                <div class="position-relative form-group">
                                                    <label for="examplePassword" class="">Password Baru</label>
                                                    <input name="password" id="password" placeholder="password baru" type="password" class="form-control" required>
                                                    <div class="invalid-feedback">
                                                        minimal 8 karakter
                                                    </div>
                                                </div>

                                                <div class="position-relative form-group">
                                                    <label for="examplePassword" class="">Konfirmasi Password Baru</label>
                                                    <input name="password" id="confirmpassword" placeholder="ketik ulang password baru" type="password" class="form-control" required>
                                                    <div class="invalid-feedback">
                                                        Password tidak sama
                                                    </div>
                                                </div>

                                                <button class="mt-1 btn btn-success btn-block">Kirim</button>
                                            </form>
                                            <script>
                                            // Example starter JavaScript for disabling form submissions if there are invalid fields
                                            (function() {
                                                'use strict';
                                                window.addEventListener('load', function() {
                                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                                    var forms = document.getElementsByClassName('needs-validation');
                                                    // Loop over them and prevent submission
                                                    var validation = Array.prototype.filter.call(forms, function(form) {
                                                        form.addEventListener('submit', function(event) {
                                                            if (form.checkValidity() === false) {
                                                                event.preventDefault();
                                                                event.stopPropagation();
                                                            }
                                                            form.classList.add('was-validated');
                                                        }, false);
                                                    });
                                                }, false);
                                            })();
                                        </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
