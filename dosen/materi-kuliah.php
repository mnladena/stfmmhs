<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademis</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Jadwal Kuliah Tahun Ajaran 2019/2020 Semester 6 - Genap
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">

                                        <div class="card-header">
                                            <div class="btn-actions-pane-right">
                                                <a href="kelas.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                            
                                        </div>

                                        <div class="card-body">

                                            <div class="row">
                                                <div class="col-md-6 col-xl-6">
                                                    <h3>Farmakologi Dasar</h3>
                                                    <div><b>Kode MK:</b> <span>MK8-031210</span></div>
                                                    <div><b>SKS:</b> <span>2 SKS</span></div>
                                                    <div><b>Sifat:</b> <span>Wajib</span></div>
                                                    <div><b>Semester:</b> <span>1</span></div>
                                                </div>

                                                <!-- <div class="col-md-6 col-xl-6">
                                                    <div class="row">
                                                        <div class="col-md-7 col-xl-7">
                                                            <div class="alert alert-info fade show">
                                                                <div class="pt-1 pb-1 text-bold">Kelas - 01FSA</div>
                                                                <div class="pt-1 pb-1 text-bold">Ruang - H.102</div>
                                                                <div class="pt-1 pb-1 text-bold">Kamis, 14.00-15.00</div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 col-xl-5">
                                                            <div class="alert alert-warning fade show text-center">
                                                                <div>Kehadiran</div>
                                                                <div class="big-text mb-2">10 dari 14</div>
                                                                <div>Ketidakhadiran</div>
                                                                <div class="big-text">0 dari 3</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->

                                            </div>

                                            <hr>

                                            <div class="">
                                                <h5>Deskripsi Mata Kuliah</h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nihil sit tenetur amet dolorum nesciunt vitae dignissimos minus, harum, natus fugiat voluptas totam ipsam officia in, ipsum odio explicabo ab.</p>

                                                <h5>Tujuan Pembelajaran</h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum nihil sit tenetur amet dolorum nesciunt vitae dignissimos minus, harum, natus fugiat voluptas totam ipsam officia in, ipsum odio explicabo ab.</p>

                                                <h5>Pertemuan Perkuliahan</h5>

                                                <div id="accordion" class="accordion-wrapper mb-3">
                                                    <div class="card">
                                                        <div id="headingOne" class="card-header">
                                                            <button type="button" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne" class="text-left m-0 p-0 btn btn-link btn-block btn-accord">
                                                                <h5 class="m-0 p-0">Pertemuan ke-1: Mengetahui Sejarah Kefarmasian</h5>
                                                                <span class="badge badge-danger">Kuis!</span>
                                                                <i class="pe-7s-angle-up-circle"></i>
                                                            </button>
                                                        </div>
                                                        <div data-parent="#accordion" id="collapseOne1" aria-labelledby="headingOne" class="collapse">
                                                            <div class="card-body">
                                                                <div>1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                                nesciunt
                                                                laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                                                sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                VHS.
                                                                </div>
                                                                <div class="materi-file mt-2">
                                                                    <a class="btn btn-danger" href="#">
                                                                        <i class="pe-7s-file"></i>
                                                                        File Materi
                                                                    </a>
                                                                    <a class="btn btn-danger" href="#">
                                                                        <i class="pe-7s-file"></i>
                                                                        File Kuis
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div id="headingTwo" class="b-radius-0 card-header">
                                                            <button type="button" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="false" aria-controls="collapseTwo" class="text-left m-0 p-0 btn btn-link btn-block btn-accord"><h5 class="m-0 p-0">Pertemuan ke-2: Farmasi di Era Perang Dunia 2</h5>
                                                                <i class="pe-7s-angle-up-circle"></i></button>
                                                        </div>
                                                        <div data-parent="#accordion" id="collapseOne2" class="collapse">
                                                            <div class="card-body">2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                                nesciunt
                                                                laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                                                sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                VHS.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div id="headingThree" class="card-header">
                                                            <button type="button" data-toggle="collapse" data-target="#collapseOne3" aria-expanded="false" aria-controls="collapseThree" class="text-left m-0 p-0 btn btn-link btn-block btn-accord"><h5 class="m-0 p-0">Pertemuan ke-3: Farmasi Modern</h5>
                                                                <i class="pe-7s-angle-up-circle"></i></button>
                                                        </div>
                                                        <div data-parent="#accordion" id="collapseOne3" class="collapse">
                                                            <div class="card-body">3. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                                                                nesciunt
                                                                laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                                                                sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                VHS.
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
