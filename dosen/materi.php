<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Materi Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Materi Perkuliahan
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Jadwal Mengajar Teori
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="align-middle mb-0 table table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode MK</th>
                                                        <th>Mata Kuliah</th>
                                                        <th>SKS</th>
                                                        <th>Jenis-Kategori</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>MPK-1202</th>
                                                        <td>AIK 1 (Kemanusiaan dan keimanan)</td>
                                                        <td>2</td>
                                                        <td>Teori - Wajib</td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-pertemuan.php"><i class="pe-7s-note"></i> Materi</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>MPK-1202</th>
                                                        <td>Aplikasi Sistem Komputer Farmasi</td>
                                                        <td>2</td>
                                                        <td>Teori - Wajib</td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="materi-pertemuan.php"><i class="pe-7s-note"></i> Materi</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>        
                                        </div>
                                            
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
