<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Bimbingan Skripsi/KTI</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Bimbingan Akademik
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Bimbingan Skripis/KTI
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Mahasiswa</th>
                                                    <th>Skrips/KTI</th>
                                                    <th>Pembimbing</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <div>NIM: 2019876654</div>
                                                            <div>Nama: Jajang Nurjaman</div>
                                                            <div>Kelas: 13A</div>
                                                            <div>Prodi: S1 Farmasi</div>
                                                        </td>
                                                        <td>
                                                            <p>Judul Proposal: Pembuatan Obat Asam Urat dengan Metode Herbal</p>
                                                            <p>Judul Skripsi/KTI: Merancang Obat Asam Urat dengan Metode Herbal</p>
                                                        </td>
                                                        <td>
                                                            <div>Pembimbing</div>
                                                            <ol>
                                                                <li>Dr. Syamsul Chaeruddin</li>
                                                                <li>Dr. M. Kamri</li>
                                                            </ol>
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="bimbingan-skripsi-detail.php"><i class="pe-7s-note2"></i> Bimbingan</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="bimbingan-skripsi-berkas.php"><i class="pe-7s-note2"></i> Berkas Skripsi/KTI</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
