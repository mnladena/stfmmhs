<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Materi Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Materi Perkuliahan
                                </div>
                            </div>
                        </div> 

                        <div class="app-page-title app-page-title-nobg">
                            <div class="page-title-wrapper">
                                <div>
                                    <div class="page-title-heading">
                                       Aplikasi Sistem Komputer Farmasi
                                    </div>
                                    <div class="page-title-subheading">
                                        Pertemuan 1
                                    </div>
                                </div>
                            </div>
                        </div> 

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Tambah Materi perkuliahan
                                        </div>
                                        <div class="card-body">
                                            <form>
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Nama MK</td>
                                                    <td width="50">:</td>
                                                    <td>Aplikasi Sistem Komputer Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Pertemuan</td>
                                                    <td>:</td>
                                                    <td>1</td>
                                                </tr>
                                                <tr>
                                                    <td>Berkas</td>
                                                    <td>:</td>
                                                    <td>
                                                        <!-- <input type="file" name=""> -->
                                                       <div id="previewupload"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="alert alert-info">
                                                            Berkas yang diupload perlu diaktifkan oleh bagian akademik agar tampil pada silabus mata kuliah
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="text-center">
                                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                                    <button class="btn btn-secondary" type="reset" data-dismiss="modal">Batal</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
