<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Penilaian Mata Kuliah
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <div class="btn-actions-pane-right">
                                                <a href="kelas.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Mata Kuliah</td>
                                                    <td>:</td>
                                                    <td>Pengantar Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode MK</td>
                                                    <td>:</td>
                                                    <td>MKB-331203</td>
                                                </tr>
                                                <tr>
                                                    <td>Kelas</td>
                                                    <td>:</td>
                                                    <td>A</td>
                                                </tr>
                                                <tr>
                                                    <td>Waktu Kuliah</td>
                                                    <td>:</td>
                                                    <td>Senin, 08.00 - 09.30</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-body">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab1" class="active nav-link">Keaktifan & Etika</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab2" class="nav-link">Pre-Post Test</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab3" class="nav-link">Laporan</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab4" class="nav-link">UAP</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                
                                                <!-- keaktifan -->
                                                <div class="tab-pane active" id="tab1" role="tabpanel">
                                                    
                                                    <div class="card-header">
                                                        Nilai Keaktifan & Etika
                                                        
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                               <tr>
                                                                    <th>No</th>
                                                                    <th>Nama Mahasiswa</th>
                                                                    <th>1</th>
                                                                    <th>2</th>
                                                                    <th>3</th>
                                                                    <th>4</th>
                                                                    <th>5</th>
                                                                    <th>6</th>
                                                                    <th>7</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Muamar Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yunus</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>

                                                </div>
                                                <!-- keaktifan -->

                                                <!-- pre-post test -->
                                                <div class="tab-pane" id="tab2" role="tabpanel">

                                                    <div class="card-header">
                                                        Nilai Pre-Post Test
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                               <tr>
                                                                    <th>No</th>
                                                                    <th>Nama Mahasiswa</th>
                                                                    <th>1</th>
                                                                    <th>2</th>
                                                                    <th>3</th>
                                                                    <th>4</th>
                                                                    <th>5</th>
                                                                    <th>6</th>
                                                                    <th>7</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Muamar Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yunus</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>
                                                    
                                                </div>
                                                <!-- pre-post test -->
                                                
                                                <!-- laporan -->
                                                <div class="tab-pane" id="tab3" role="tabpanel">

                                                   <div class="card-header">
                                                        Nilai Laporan
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                               <tr>
                                                                    <th>No</th>
                                                                    <th>Nama Mahasiswa</th>
                                                                    <th>1</th>
                                                                    <th>2</th>
                                                                    <th>3</th>
                                                                    <th>4</th>
                                                                    <th>5</th>
                                                                    <th>6</th>
                                                                    <th>7</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Muamar Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yunus</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-nilai"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>

                                                </div>
                                                <!-- laporan -->

                                                <!-- UAP -->
                                                <div class="tab-pane" id="tab4" role="tabpanel">

                                                   <div class="card-header">
                                                        Nilai UAP
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                               <tr>
                                                                    <th>No</th>
                                                                    <th>Nama Mahasiswa</th>
                                                                    <th>UTS</th>
                                                                    <th>UAS</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-uap"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Muamar Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-uap"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Salahuddin Yunus</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-uap"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>
                                                                        <a href="absensi-detail.php">Yusuf</a>
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        -
                                                                    </td>
                                                                    <td>
                                                                        <button class="btn-transition btn btn-primary" data-toggle="modal" data-target=".input-uap"><i class="pe-7s-pen"></i> Input Nilai</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>

                                                </div>
                                                <!-- UAP -->

                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>

                    <!-- input nilai -->
                    <div class="modal fade input-nilai" tabindex="-1" role="dialog" aria-labelledby="inputNilai" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Input Nilai</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <form class="form-custom">
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nama</label>
                                            <div class="col-sm-8">
                                                <span class="text-form">
                                                    Salahuddin Yusuf
                                                </span>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai Pertemuan 1</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai Pertemuan 2</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai Pertemuan 3</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai Pertemuan 4</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai Pertemuan 5</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai Pertemuan 6</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai Pertemuan 7</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="position-relative row form-check mt-5">
                                            <div class="col-sm-12 text-center">
                                                <button class="btn btn-primary" type="submit">Simpan</button>
                                                <button class="btn btn-secondary" type="reset" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- input nilai UAP-->
                    <div class="modal fade input-uap" tabindex="-1" role="dialog" aria-labelledby="inputUAP" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Input Nilai</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <form class="form-custom">
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nama</label>
                                            <div class="col-sm-8">
                                                <span class="text-form">
                                                    Salahuddin Yusuf
                                                </span>
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai UTS</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <label for="example" class="col-sm-4 col-form-label">Nilai UAP</label>
                                            <div class="col-sm-2">
                                                <input name="" id="example" placeholder="" type="" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="position-relative row form-check mt-5">
                                            <div class="col-sm-12 text-center">
                                                <button class="btn btn-primary" type="submit">Simpan</button>
                                                <button class="btn btn-secondary" type="reset" data-dismiss="modal">Batal</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
