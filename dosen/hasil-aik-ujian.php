<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="breadcrumb-item"><a href="">Pengujian Sidang</a></li>
                            <li class="active breadcrumb-item" aria-current="page">AIK</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Pengujian Sidang
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            AIK
                                             <div class="btn-actions-pane-right">
                                                <a href="pengujian-sidang-aik.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1-Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1 Farmasi</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-header">
                                            Kategori Materi : Al-Islam
                                        </div>
                                        <div class="card-body">

                                                    <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                        <thead>
                                                           <tr>
                                                                <th>No</th>
                                                                <th>Penilaian</th>
                                                                <th width="200">Nilai Angka</th>
                                                                <th width="150">Nilai Huruf</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td width="30">1</td>
                                                                <td>
                                                                    Baca Al Qur'an dan Tajwid
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                                <td>
                                                                   B
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">2</td>
                                                                <td>
                                                                    Hafalan Surat-surat Pendek
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                                <td>
                                                                   B
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30">3</td>
                                                                <td>
                                                                    Doa-doa Harian
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                                <td>
                                                                   B
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2">Total Angka</td>
                                                                <td>237</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">Rata-rata</td>
                                                                <td>79</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">Nilai Huruf</td>
                                                                <td>B</td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div class="alert alert-success mt-3">
                                                        Dinyatakan <h4>Lulus</h4>
                                                    </div>
                                                    <div class="alert alert-danger mt-3">
                                                        Dinyatakan <h4>Tidak Lulus</h4>
                                                    </div>
                                                </div>

                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>


                    
