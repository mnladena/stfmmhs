<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="breadcrumb-item"><a href="">Pengujian Sidang</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Skripsi/KTI</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Pengujian Sidang
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Skripsi/KTI
                                        </div>
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td>
                                                    <td width="50">:</td>
                                                    <td>S1-Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Program Studi</td>
                                                    <td>:</td>
                                                    <td>S1 Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Judul Seminar</td>
                                                    <td>:</td>
                                                    <td>Pembuatan Obat Asam Urat dengan Metode Herbal</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card">
                                        <div class="card-header">
                                            <div class="btn-actions-pane-right">
                                                <a href="pengujian-sidang-skripsi.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item"><a data-toggle="tab" href="#tab1" class="active nav-link">Hasil Sidang Skripsi/KTI</a></li>
                                                <li class="nav-item"><a data-toggle="tab" href="#tab2" class="nav-link">Hasil Ujian Tugas Akhir Skripsi/KTI</a></li>
                                            </ul>
                                            <div class="tab-content">

                                                <!-- Pembimbing 2 -->
                                                <div class="tab-pane active" id="tab1" role="tabpanel">

                                                   <div class="card-header">
                                                        Hasil Ujian Sidang Skripsi/KTI
                                                        
                                                    </div>
                                                    <div class="card-body">
                                                        <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    Total nilai dosen pembimbing 1
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Total nilai dosen pembimbing 2
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Total nilai dosen penguji 1
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Total nilai dosen penguji 2
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Total nilai keseluruhan
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td>Nilai Rata-rata</td>
                                                                <td>85.25</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nilai Akhir</td>
                                                                <td>A</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div class="alert alert-success mt-3">
                                                        Dinyatakan <h4>Lulus</h4>
                                                    </div>
                                                    <div class="alert alert-danger mt-3">
                                                        Dinyatakan <h4>Tidak Lulus</h4>
                                                    </div>
                                                    </div>

                                                </div>
                                                <!-- Pembimbing 2 -->
                                                
                                                <!-- Pembimbing 1 -->
                                                <div class="tab-pane" id="tab2" role="tabpanel">
                                                    
                                                    <div class="card-header">
                                                        Hasil Ujian Tugas Akhir Skripsi/KTI
                                                        
                                                    </div>
                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-bordered table-hover">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    Total nilai dosen pembimbing 1
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Total nilai dosen pembimbing 2
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Total nilai keseluruhan
                                                                </td>
                                                                <td>
                                                                    79
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td>Nilai Rata-rata</td>
                                                                <td>85.25</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nilai Akhir</td>
                                                                <td>A</td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div class="alert alert-success mt-3">
                                                        Dinyatakan <h4>Lulus</h4>
                                                    </div>
                                                    <div class="alert alert-danger mt-3">
                                                        Dinyatakan <h4>Tidak Lulus</h4>
                                                    </div>
                                                        
                                                    </div>

                                                </div>
                                                <!-- Pembimbing 1 -->

                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>

                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>

                    <!-- simpan perubahan -->
                    <div class="modal fade tambah-bimbingan" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Konfirmasi</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <div class="text-center">
                                        Apakah Anda yakin ingin menyimpan penilaian?
                                        <br>
                                        penilaian tidak bisa diubah kembali setelah disimpan
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
