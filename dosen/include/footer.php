<div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner text-center">
                                <span class="text-center">
                                    Sekolah Tinggi Farmasi Muhammadiyah Tangerang
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
        </div>
    </div>
<script type="text/javascript" src="assets/scripts/jquery.min.js"></script>
<script type="text/javascript" src="assets/scripts/main.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $("#previewupload").uploadFile({
    url:"upload.php",
    fileName:"myfile",
    multiple:false,
    maxFileCount:1,
    dragDrop:false,
    acceptFiles:"image/*",
    showPreview:true,
    showDelete: true,
    uploadStr:"Browse Image",
     // previewHeight: "150px",
     previewWidth: "150px",
    });
});
</script>
</body>
</html>