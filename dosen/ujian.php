<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Ujian</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Jadwal Ujian TA 2019/2020 Semester 6 - Genap
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun Akademik/Semester</td>
                                                    <td>:</td>
                                                    <td>2015/2016 Ganjil - 1</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card card">
                                    
                                    <div class="card-header">
                                        <ul class="nav">
                                            <li class="nav-item"><a data-toggle="tab" href="#ujian" class="active nav-link">Jadwal Ujian</a></li>
                                            <li class="nav-item"><a data-toggle="tab" href="#ujian-remedial" class="nav-link">Ujian Remedial</a></li>
                                            <li class="nav-item"><a data-toggle="tab" href="#jadwal-remedial" class="nav-link">Jadwal Ujian Remedial</a></li>
                                        </ul>
                                    </div>
                                    
                                    <div class="card-body">

                                    <div class="tab-content">

                                        <div class="tab-pane active" id="ujian" role="tabpanel">

                                                <div class="main-card mb-3 card card-border">
                                                    <h5 class="card-title ml-sm-4 mt-sm-4">Jadwal Ujian Teori</h5>
                                                    <div class="divider"></div>
                                                    <div class="card-header action">
                                                                <form class="form-inline">
                                                                    <div class="form-group">
                                                                        <label class="mr-sm-2">Periode</label>
                                                                        <select class="form-control" name="" id="">
                                                                            <option>2019/2020 - Genap - UTS Semester 6</option>
                                                                        </select>
                                                                    </div>
                                                                </form>
                                                            <div class="btn-actions-pane-right">
                                                                <a href="kartu/kartu-ujian.html" class="btn-transition btn btn-outline-primary"><i class="pe-7s-download"></i> Download Kartu Ujian</a>
                                                            </div>
                                                    </div>

                                                    <div class="card-body">

                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Tanggal</th>
                                                                <th>Waktu</th>
                                                                <th>Ruang</th>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Kelas</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        
                                                    </div>

                                                    <div class="card-header">
                                                       Jadwal Ujian Praktikum
                                                    </div>

                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Tanggal</th>
                                                                <th>Waktu</th>
                                                                <th>Ruang</th>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Kelas</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="tab-pane" id="ujian-remedial" role="tabpanel">

                                                <div class="main-card mb-3 card card-border">
                                                    <h5 class="card-title ml-sm-4 mt-sm-4">Mata Kuliah Yang Tersedia</h5>
                                                    <div class="divider"></div>
                                                    <div class="card-header action">
                                                        <form class="form-inline">
                                                            <div class="form-group">
                                                                <label class="mr-sm-2">Periode</label>
                                                                <select class="form-control" name="" id="">
                                                                    <option>2019/2020 - Genap - UTS Semester 6</option>
                                                                </select>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>SKS</th>
                                                                <th>Nilai Akhir</th>
                                                                <th>Status Remedial</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>MPK-1202</td>
                                                                    <td>Kimia Dasar 1</td>
                                                                    <td>2</td>
                                                                    <td>C</td>
                                                                    <td>Tidak Tersedia</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>MPK-1202</td>
                                                                    <td>Kimia Dasar 2</td>
                                                                    <td>2</td>
                                                                    <td>D</td>
                                                                    <td>Tersedia</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                        <div class="divider"></div>

                                                        <button class="btn mb-4 btn btn-primary" data-toggle="modal" data-target=".daftar-remedial">Daftar Remedial</button>
                                                        <div class="table-responsive">
                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Ujian Remedial</th>
                                                                <th>Status Ka Prodi</th>
                                                                <th>Status Dosen PA</th>
                                                                <th>Keputusan</th>
                                                                <th>Status Bayar</th>
                                                                <th>Aksi</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>
                                                                        <div>- Kimia Dasar</div>
                                                                        <div>- Biologi Sel</div>
                                                                    </td>
                                                                    <td>
                                                                        <span class="status status-uncheck"><i class="pe-7s-close-circle"></i></span>
                                                                    </td>
                                                                    <td><span class="status status-check"><i class="pe-7s-check"></i></span></td>
                                                                    <td>Ditolak</td>
                                                                    <td>Belum Bayar</td>
                                                                    <td>
                                                                        <button class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" data-toggle="modal" data-target=".remedial-tolak"><i class="pe-7s-look"></i> Info</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>
                                                                        <div>- Kimia Dasar 2</div>
                                                                    </td>
                                                                    <td>
                                                                        <span class="status status-wait"><i class="pe-7s-clock"></i></span>
                                                                    </td>
                                                                    <td><span class="status status-check"><i class="pe-7s-check"></i></span></td>
                                                                    <td>Menunggu</td>
                                                                    <td>Belum Bayar</td>
                                                                    <td>
                                                                        <button class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" data-toggle="modal" data-target=".remedial-tunggu"><i class="pe-7s-look"></i> Info</button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>
                                                                        <div>- Kimia Dasar 2</div>
                                                                    </td>
                                                                    <td><span class="status status-check"><i class="pe-7s-check"></i></span></td>
                                                                    <td><span class="status status-check"><i class="pe-7s-check"></i></span></td>
                                                                    <td>Diterima</td>
                                                                    <td>Sudah Bayar</td>
                                                                    <td>
                                                                        <button class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" data-toggle="modal" data-target=".remedial-sukses"><i class="pe-7s-look"></i> Info</button>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                        
                                                    </div>

                                                </div>
                                                
                                            </div>

                                            <div class="tab-pane" id="jadwal-remedial" role="tabpanel">

                                                <div class="main-card mb-3 card card-border">
                                                    <h5 class="card-title ml-sm-4 mt-sm-4">Jadwal Ujian Teori</h5>
                                                    <div class="divider"></div>
                                                    <div class="card-header action">
                                                                <form class="form-inline">
                                                                    <div class="form-group">
                                                                        <label class="mr-sm-2">Periode</label>
                                                                        <select class="form-control" name="" id="">
                                                                            <option>2019/2020 - Genap - UTS Semester 6</option>
                                                                        </select>
                                                                    </div>
                                                                </form>
                                                            <div class="btn-actions-pane-right">
                                                                <button class="btn-transition btn btn-outline-primary"><i class="pe-7s-download"></i> Download Kartu Ujian</button>
                                                            </div>
                                                    </div>

                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                        <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Tanggal</th>
                                                                <th>Waktu</th>
                                                                <th>Ruang</th>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Kelas</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                        
                                                    </div>

                                                    <div class="card-header">
                                                       Jadwal Ujian Praktikum
                                                    </div>

                                                    <div class="card-body">

                                                        <div class="alert alert-primary text-center">
                                                            Tidak ada jadwal ujian praktikum
                                                        </div>

                                                        <!-- <table class="align-middle mb-0 table table-striped table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Tanggal</th>
                                                                <th>Waktu</th>
                                                                <th>Ruang</th>
                                                                <th>Kode MK</th>
                                                                <th>Mata Kuliah</th>
                                                                <th>Kelas</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Jumat 06/12/2019</td>
                                                                    <td>08.00-09.30</td>
                                                                    <td>L203</td>
                                                                    <td>MPK-1202</td>
                                                                    <td>AIK 1(Kemanusiaan dan Keimanan)</td>
                                                                    <td>2018A</td>
                                                                </tr>
                                                            </tbody>
                                                        </table> -->
                                                        
                                                    </div>

                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                    </div>

                                </div>
                                
                            </div>
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                     <!-- modal -->

                    <!-- daftar remedial -->
                    <div class="modal fade daftar-remedial" tabindex="-1" role="dialog" aria-labelledby="daftarRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <table class="align-middle mb-0 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                                <th>Pilih</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                                <td>
                                                    <div class="custom-checkbox custom-control">
                                                        <input type="checkbox" id="check1" class="custom-control-input">
                                                        <label class="custom-control-label" for="check1"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                                <td>
                                                    <div class="custom-checkbox custom-control">
                                                        <input type="checkbox" id="check2" class="custom-control-input">
                                                        <label class="custom-control-label" for="check2"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="2">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div class="mt-3 text-muted">Catatan: Mata kuliah yang dipilih  untuk remedial harus mendapatkan persetujuan dari Ketua Prodi dan Dosen Pembimbing Akademik</div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- info remedial ditolak -->
                    <div class="modal fade remedial-tolak" tabindex="-1" role="dialog" aria-labelledby="tolakRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Informasi Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <div class="alert alert-danger text-center"><h4>DITOLAK</h4></div>

                                    <table class="align-middle mb-2 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="1">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div>
                                        <b>Catatan Ketua Prodi (Dr. Miftah Farid)</b>
                                        <div class="text-muted"><span class="text-danger">Ditolak</span> kamu tidak perlu mengulang mata kuliah ini</div>
                                    </div>
                                    <div class="divider"></div>
                                    <div>
                                        <b>Catatan Dosen Pembimbing Akademi (Dr. Anwar Fuadi)</b>
                                        <div class="text-muted"><span class="text-success">Diterima</span> silakan lanjutkan</div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- info remedial tunggu -->
                    <div class="modal fade remedial-tunggu" tabindex="-1" role="dialog" aria-labelledby="tolakRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Informasi Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <div class="alert alert-secondary text-center"><h4>MENUNGGU</h4></div>

                                    <table class="align-middle mb-2 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="1">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div>
                                        <b>Catatan Ketua Prodi (Dr. Miftah Farid)</b>
                                        <div class="text-muted"><span class="text-danger">Ditolak</span> kamu tidak perlu mengulang mata kuliah ini</div>
                                    </div>
                                    <div class="divider"></div>
                                    <div>
                                        <b>Catatan Dosen Pembimbing Akademi (Dr. Anwar Fuadi)</b>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- info remedial diterima -->
                    <div class="modal fade remedial-sukses" tabindex="-1" role="dialog" aria-labelledby="tolakRemedial" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Informasi Pendaftaran Ujian Remedial</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="text-center">Kartu Rencana Studi Semester Remedial</h5>

                                    <table class="mb-4 table table-data table-borderless">
                                        <tr>
                                            <td>NIM</td>
                                            <td>:</td>
                                            <td>11092384</td>
                                        </tr>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td>Mikael Silvestre</td>
                                        </tr>
                                        <tr>
                                            <td>Tahun Akademik</td>
                                            <td>:</td>
                                            <td>2015/2016</td>
                                        </tr>
                                        <tr>
                                            <td>Semester</td> 
                                            <td>:</td>
                                            <td>Semester 6</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pendaftaran</td>
                                            <td>:</td>
                                            <td>10 februari 2020</td>
                                        </tr>
                                    </table>

                                    <div class="alert alert-success text-center"><h4>DITERIMA</h4></div>

                                    <table class="align-middle mb-2 table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>Kode MK</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 1</td>
                                                <td>2</td>
                                            </tr>
                                            <tr>
                                                <td>MPK-1202</td>
                                                <td>Kimia Dasar 2</td>
                                                <td>2</td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="total">
                                            <tr>
                                                <td colspan="2">Total SKS</td>
                                                <td colspan="1">75</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div>
                                        <b>Catatan Ketua Prodi (Dr. Miftah Farid)</b>
                                        <div class="text-muted"><span class="text-success">Diterima</span> sudah sesuai, silahkan lanjutkan</div>
                                    </div>
                                    <div class="divider"></div>
                                    <div>
                                        <b>Catatan Dosen Pembimbing Akademi (Dr. Anwar Fuadi)</b>
                                        <div class="text-muted"><span class="text-success">Diterima</span> silakan lanjutkan</div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
