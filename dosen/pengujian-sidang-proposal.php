<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="breadcrumb-item"><a href="">Pengujian Sidang</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Proposal</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Pengujian Sidang
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <h4>Sidang Proposal Skripsi/KTI</h4>
                                        </div>
                                        <div class="card-header">
                                            Tahun Akademik: 2019/2020 - Semester 8
                                            <div class="btn-actions-pane-right">
                                                <select class="form-control-sm form-control">
                                                    <option>Tampilkan Data</option>
                                                    <option>Tidak Tampilkan Data</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-baseline mb-0 table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Mahasiswa</th>
                                                    <th>Proposal Skripsi/KTI</th>
                                                    <th>Informasi Sidang</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <div class="font-weight-bold">NIM:</div> 
                                                            <div class="mb-2">2019876654</div>

                                                            <div class="font-weight-bold">Nama:</div> 
                                                            <div class="mb-2">Jajang Nurjaman</div>

                                                            <div class="font-weight-bold">Prodi:</div> 
                                                            <div class="mb-2">S1 Farmasi</div>
                                                        </td>
                                                        <td>
                                                            <div class="font-weight-bold">Judul Skripsi/KTI:</div> 
                                                            <div class="mb-3">Merancang Obat Asam Urat dengan Metode Herbal</div>
                                                            <div class="font-weight-bold">Pembimbing</div>
                                                            <ol>
                                                                <li>Dr. Syamsul Chaeruddin</li>
                                                                <li>Dr. M. Kamri</li>
                                                            </ol>
                                                        </td>
                                                        <td>
                                                            <p>
                                                               07/07/2020 - 09.00
                                                            </p>
                                                            <div class="font-weight-bold">Penguji</div>
                                                            <ol>
                                                                <li>Dr. Syamsul Chaeruddin</li>
                                                                <li>Dr. M. Kamri</li>
                                                            </ol>
                                                        </td>
                                                        <td>
                                                            Belum Menguji
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>
                                                            <div class="font-weight-bold">NIM:</div> 
                                                            <div class="mb-2">2019876654</div>

                                                            <div class="font-weight-bold">Nama:</div> 
                                                            <div class="mb-2">Jajang Nurjaman</div>
                                                            
                                                            <div class="font-weight-bold">Prodi:</div> 
                                                            <div class="mb-2">S1 Farmasi</div>
                                                        </td>
                                                         <td>
                                                            <div class="font-weight-bold">Judul Skripsi/KTI:</div> 
                                                            <div class="mb-3">Merancang Obat Asam Urat dengan Metode Herbal</div>
                                                            <div class="font-weight-bold">Pembimbing</div>
                                                            <ol>
                                                                <li>Dr. Syamsul Chaeruddin</li>
                                                                <li>Dr. M. Kamri</li>
                                                            </ol>
                                                        </td>
                                                        <td>
                                                            <p>
                                                               07/07/2020 - 09.00
                                                            </p>
                                                            <div class="font-weight-bold">Penguji</div>
                                                            <ol>
                                                                <li>Dr. Syamsul Chaeruddin</li>
                                                                <li>Dr. M. Kamri</li>
                                                            </ol>
                                                        </td>
                                                        <td>
                                                            Belum Menguji
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="sidang-proposal-ujian.php"><i class="pe-7s-note2"></i> Ujian</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>
                                                            <div class="font-weight-bold">NIM:</div> 
                                                            <div class="mb-2">2019876654</div>

                                                            <div class="font-weight-bold">Nama:</div> 
                                                            <div class="mb-2">Jajang Nurjaman</div>
                                                            
                                                            <div class="font-weight-bold">Prodi:</div> 
                                                            <div class="mb-2">S1 Farmasi</div>
                                                        </td>
                                                         <td>
                                                            <div class="font-weight-bold">Judul Skripsi/KTI:</div> 
                                                            <div class="mb-3">Merancang Obat Asam Urat dengan Metode Herbal</div>
                                                            <div class="font-weight-bold">Pembimbing</div>
                                                            <ol class="list-unstyled">
                                                                <li>Dr. Syamsul Chaeruddin</li>
                                                                <li>Dr. M. Kamri</li>
                                                            </ol>
                                                        </td>
                                                        <td>
                                                            <p>
                                                               07/07/2020 - 09.00
                                                            </p>
                                                            <div class="font-weight-bold">Penguji</div>
                                                            <ol>
                                                                <li>Dr. Syamsul Chaeruddin</li>
                                                                <li>Dr. M. Kamri</li>
                                                            </ol>
                                                        </td>   
                                                        <td>
                                                            Sudah Menguji
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="sidang-proposal-ujian.php"><i class="pe-7s-note2"></i> Ujian</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="hasil-proposal-ujian.php"><i class="pe-7s-note2"></i> Nilai</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
