<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Menu Utama</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Halaman Utama</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Pengumuman
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body title-text">
                                            <h1>Pelantikan dan Serah Terima Jabatan WAKIL KETUA STFM 2019-2023</h1>

                                            <div class="title-text__date mb-4">26 Desember 2019 &bull; admin</div>

                                            <p>
                                                سْمِ اللهِ الرَّحْمٰنِ الرَّحيمِ

                                                    <br>
                                                    <br>


                                                    Assalamua’laikum wr wb

                                                    <br>
                                                    <br>


                                                    pada tanggal 26 desember 2019, bertempat di Ruang Kelas STFM, Acara Pelantikan dan Serah Terima Jabatan Wakil Ketua STFM PERIODE 2019-2023 dilaksanakan, acara berjalan dengan lancar. di acara ini turut hadir Ketua PWM Dr.H.M.Syamsudin, M.Pd, dan BPH.
                                            </p>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
