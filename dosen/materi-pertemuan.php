<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Materi Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Materi Perkuliahan
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Aplikasi Sistem Komputer Farmasi
                                            <div class="btn-actions-pane-right">
                                                <a href="kelas.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="align-middle mb-0 table table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Pertemuan</th>
                                                        <th>Mata Kuliah</th>
                                                        <th>Kuis</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>1</th>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-info" href=""><i class="pe-7s-download"></i> Download</a>
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-info" href=""><i class="pe-7s-download"></i> Download</a>
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="list-materi-kuliah.php"><i class="pe-7s-note"></i> Materi Kuliah</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="list-materi-kuis.php"><i class="pe-7s-pen"></i> Kuis</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>2</th>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-info" href=""><i class="pe-7s-download"></i> Download</a>
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-info" href=""><i class="pe-7s-download"></i> Download</a>
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="list-materi-kuliah.php"><i class="pe-7s-note"></i> Materi Kuliah</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="list-materi-kuis.php"><i class="pe-7s-pen"></i> Kuis</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>3</th>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-info" href=""><i class="pe-7s-download"></i> Download</a>
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-info" href=""><i class="pe-7s-download"></i> Download</a>
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="list-materi-kuliah.php"><i class="pe-7s-note"></i> Materi Kuliah</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="list-materi-kuis.php"><i class="pe-7s-pen"></i> Kuis</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>        
                                        </div>
                                            
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
