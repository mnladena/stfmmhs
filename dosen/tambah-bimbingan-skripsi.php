<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Bimbingan Akademik</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Tambah Bimbingan Skripsi/KTI
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>Judul Proposal</td>
                                                    <td>:</td>
                                                    <td>Pembuatan Obat Asam Urat dengan Metode Herbal</td>
                                                </tr>
                                                <tr>
                                                    <td>Judul Skripsi/KTI</td>
                                                    <td>:</td>
                                                    <td>Pembuatan Obat Asam Urat dengan Metode Herbal</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Pembimbing</td>
                                                    <td width="50">:</td>
                                                    <td>Dr. Syamsul Chaeudin</td>
                                                </tr>
                                                <tr>
                                                    <td>Bimbingan Ke</td>
                                                    <td>:</td>
                                                    <td>2</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-top">Isi Bimbingan</td>
                                                    <td class="align-top">:</td>
                                                    <td>
                                                        <form>
                                                            <div class="position-relative form-group">
                                                                <textarea name="text" id="exampleText" class="form-control textarea-lg"></textarea>
                                                                <div class="alert alert-danger mt-3" role="alert">Form Harus diisi!</div>
                                                            </div>
                                                            <div class="text-center">
                                                                <a href="" data-toggle="modal" data-target=".tambah-bimbingan" class="btn btn-primary">Simpan</a>
                                                                <button class="btn btn-secondary" type="reset">Batal</button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>

                    <!-- simpan perubahan -->
                    <div class="modal fade tambah-bimbingan" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" id="">Konfirmasi</div>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    
                                    <div class="text-center">
                                        Apakah Anda yakin ingin menyimpan isi bimbingan?
                                        <br>
                                        Isi bimbingan tidak bisa diubah kembali setelah disimpan
                                    </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
