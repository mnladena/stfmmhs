<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Absensi Perkuliahan
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            <div class="btn-actions-pane-right">
                                                <a href="kelas.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Mata Kuliah</td>
                                                    <td>:</td>
                                                    <td>Pengantar Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode MK</td>
                                                    <td>:</td>
                                                    <td>MKB-331203</td>
                                                </tr>
                                                <tr>
                                                    <td>Kelas</td>
                                                    <td>:</td>
                                                    <td>A</td>
                                                </tr>
                                                <tr>
                                                    <td>Waktu Kuliah</td>
                                                    <td>:</td>
                                                    <td>Senin, 08.00 - 09.30</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Mahasiswa</th>
                                                    <th>1</th>
                                                    <th>2</th>
                                                    <th>3</th>
                                                    <th>4</th>
                                                    <th>5</th>
                                                    <th>6</th>
                                                    <th>7</th>
                                                    <th>8</th>
                                                    <th>9</th>
                                                    <th>10</th>
                                                    <th>11</th>
                                                    <th>12</th>
                                                    <th>13</th>
                                                    <th>14</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <a href="absensi-detail.php">Salahuddin Yusuf</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>
                                                            <a href="absensi-detail.php">Dinda Kirana</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>
                                                            <a href="absensi-detail.php">Jainudin Yusuf</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4</td>
                                                        <td>
                                                            <a href="absensi-detail.php">Raden</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>5</td>
                                                        <td>
                                                            <a href="absensi-detail.php">Yono</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>6</td>
                                                        <td>
                                                            <a href="absensi-detail.php">Yusuf Salah</a>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" class="check-input">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
