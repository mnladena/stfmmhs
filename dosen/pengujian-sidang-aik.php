<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="breadcrumb-item"><a href="">Pengujian Sidang</a></li>
                            <li class="active breadcrumb-item" aria-current="page">AIK</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Pengujian Sidang
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            AIK
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-baseline mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Mahasiswa</th>
                                                    <th>Jadwal Sidang</th>
                                                    <th>Penguji</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>
                                                            <div class="font-weight-bold">NIM:</div> 
                                                            <div class="mb-2">2019876654</div>

                                                            <div class="font-weight-bold">Nama:</div> 
                                                            <div class="mb-2">Jajang Nurjaman</div>
                                                            
                                                            <div class="font-weight-bold">Prodi:</div> 
                                                            <div class="mb-2">S1 Farmasi</div>
                                                        </td>
                                                        <td>
                                                            07/07/2020
                                                            <div>09.00</div>
                                                        </td>
                                                        <td>
                                                            <div class="font-weight-bold">Penguji 1</div>
                                                            <div class="mb-2">Muhammad Anwar S.Pd</div>

                                                            <div class="font-weight-bold">Penguji 2</div>
                                                            <div class="mb-2">Wardi S.Pd</div>
                                                        </td>
                                                        <td>
                                                            Belum Menguji
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>
                                                            <div class="font-weight-bold">NIM:</div> 
                                                            <div class="mb-2">2019876654</div>

                                                            <div class="font-weight-bold">Nama:</div> 
                                                            <div class="mb-2">Jajang Nurjaman</div>
                                                            
                                                            <div class="font-weight-bold">Prodi:</div> 
                                                            <div class="mb-2">S1 Farmasi</div>
                                                        </td>
                                                        <td>
                                                            07/07/2020
                                                            <div>09.00</div>
                                                        </td>
                                                        <td>
                                                            <div class="font-weight-bold">Penguji 1</div>
                                                            <div class="mb-2">Anwar S.Pd</div>
                                                            <div class="font-weight-bold">Penguji 2</div>
                                                            <div class="mb-2">Marjani S.Pd</div>
                                                        </td>
                                                        <td>
                                                            Belum Menguji
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="sidang-aik-ujian.php"><i class="pe-7s-note2"></i> Ujian</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>
                                                            <div class="font-weight-bold">NIM:</div> 
                                                            <div class="mb-2">2019876654</div>

                                                            <div class="font-weight-bold">Nama:</div> 
                                                            <div class="mb-2">Jajang Nurjaman</div>
                                                            
                                                            <div class="font-weight-bold">Prodi:</div> 
                                                            <div class="mb-2">S1 Farmasi</div>
                                                        </td>
                                                        <td>
                                                            07/07/2020
                                                            <div>09.00</div>
                                                        </td>
                                                        <td>
                                                            <div class="font-weight-bold">Penguji 1</div>
                                                            <div class="mb-2">Broto Seno S.Pd</div>

                                                            <div class="font-weight-bold">Penguji 2</div>
                                                            <div class="mb-2">Rusdiawan Santoso S.Pd</div>
                                                        </td>
                                                        <td>
                                                            Sudah Menguji
                                                        </td>
                                                        <td>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="sidang-aik-ujian.php"><i class="pe-7s-note2"></i> Ujian</a>
                                                            <a class="btn-sm mr-2 mb-2 mt-2 btn btn-outline-info" href="hasil-aik-ujian.php"><i class="pe-7s-note2"></i> Nilai</a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
