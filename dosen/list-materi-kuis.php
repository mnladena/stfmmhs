<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademis</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Materi Perkuliahan</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Materi Perkuliahan
                                </div>
                            </div>
                        </div> 

                        <div class="app-page-title app-page-title-nobg">
                            <div class="page-title-wrapper">
                                <div>
                                    <div class="page-title-heading">
                                       Aplikasi Sistem Komputer Farmasi
                                    </div>
                                    <div class="page-title-subheading">
                                        Pertemuan 1
                                    </div>
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">



                            <div class="row">

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">

                                        <div class="card-header">
                                            <div class="btn-actions-pane-left">
                                                <a href="tambah-materi.php" class="btn-transition btn btn-primary"><i class="pe-7s-plus"></i> Tambah Materi Kuis</a>
                                            </div>
                                            <div class="btn-actions-pane-right">
                                                <a href="materi-kuliah.php" class="btn-transition btn btn-outline-primary"><i class="pe-7s-back"></i> Kembali</a>
                                            </div>
                                            
                                        </div>

                                        <div class="card-body">

                                            <div class="row">

                                                <div class="col-md-12 col-xl-12">

                                                    <div class="main-card mb-3 card card-border">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table class="align-middle mb-0 table table-striped table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>No</th>
                                                                        <th>Uploader</th>
                                                                        <th>Tgl Upload</th>
                                                                        <th>Tgl Aktif</th>
                                                                        <th>Berkas</th>
                                                                        <th>Status</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td>Salahudin Yusuf</th>
                                                                        <td>15/06/2020</td>
                                                                        <td>2020/06/2020</td>
                                                                        <td>
                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <span class="text-success">Aktif</span>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>        
                                                        </div>
                                                            
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                            
                                        </div>

                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
