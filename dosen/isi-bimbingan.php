<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Bimbingan Akademik</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Bimbingan Akademik
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Bimbingan</td>
                                                    <td>:</td>
                                                    <td>3</td>
                                                </tr>
                                                <tr>
                                                    <td>Bimbingan Ke</td>
                                                    <td>:</td>
                                                    <td>3</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <form>
                                                <div class="position-relative form-group"><label for="exampleText" class="">Isian Catatan Bimbingan</label><textarea name="text" id="exampleText" class="form-control textarea-lg"></textarea></div>
                                                <div class="text-center">
                                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                                    <button class="btn btn-secondary" type="reset" data-dismiss="modal">Batal</button>
                                                </div>
                                            </form>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
