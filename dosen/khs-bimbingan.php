<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Bimbingan Akademik</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    Bimbingan Akademik
                                </div>
                            </div>
                        </div>   

                        <div class="main-content">

                            <div class="row">

                                 <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>NIM</td>
                                                    <td width="50">:</td>
                                                    <td>0201986781</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Mahasiswa</td>
                                                    <td>:</td>
                                                    <td>Dodi Cahyadi</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Bimbingan</td>
                                                    <td>:</td>
                                                    <td>3</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-header">
                                            Nilai Mata Kuliah Teori
                                            <div class="btn-actions-pane-right">
                                                <div class="position-relative form-group mt-3 ml-5">
                                                    <select type="select" id="exampleCustomSelect" name="customSelect" class="custom-select">
                                                        <option value="">Tahun Ajaran</option>
                                                        <option>2015/2016 - Ganjil</option>
                                                        <option>2016/2017 - Genap</option>
                                                        <option>2017/2018 - Ganjil</option>
                                                        <option>2018/2019 - Genap</option>
                                                        <option>2019/2020 - Ganjil</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode MK</th>
                                                    <th>Mata Kuliah</th>
                                                    <th>SKS</th>
                                                    <th>Nilai</th>
                                                    <th>Nilai Kredit</th>
                                                    <th>Ket L/TL</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>MPK-1202</td>
                                                        <td>Kimia Dasar</td>
                                                        <td>2</td>
                                                        <td>A</td>
                                                        <td>8</td>
                                                        <td>
                                                            L
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>MPK-1202</td>
                                                        <td>Fisika Dasar</td>
                                                        <td>3(1)</td>
                                                        <td>A</td>
                                                        <td>8</td>
                                                        <td>
                                                            TL
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                        <div class="card-header">
                                            Nilai Mata Kuliah Praktikum
                                        </div>
                                        <div class="card-body">
                                            <div class="table-responsive">
                                            <table class="align-middle mb-0 table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kode MK</th>
                                                    <th>Mata Kuliah</th>
                                                    <th>SKS</th>
                                                    <th>Nilai</th>
                                                    <th>Nilai Kredit</th>
                                                    <th>Ket L/TL</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>MPK-1202</td>
                                                        <td>Fisika Dasar</td>
                                                        <td>3</td>
                                                        <td>A</td>
                                                        <td>4</td>
                                                        <td>
                                                            L
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>
                                
                            </div>
                            
                        </div>         

                    </div>

                    <?php include "include/footer.php";?>
