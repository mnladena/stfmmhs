<?php include "include/head.php" ?>
<?php include "include/header.php" ?>

                   
        <div class="app-main">

                <?php include "include/sidemenu.php";?>

                <div class="app-main__outer">

                    <nav class="" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item"><a href="">Akademik</a></li>
                            <li class="active breadcrumb-item" aria-current="page">Sidang</li>
                        </ol>
                    </nav>

                    <div class="app-main__inner">

                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    SKRIPSI
                                </div>
                            </div>
                        </div>   

                        <div class="main-content"> 

                            <div class="row">

                                <div class="col-md-12 col-xl-12">
                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">
                                            <table class="mb-0 table table-data table-borderless">
                                                <tr>
                                                    <td>Jurusan/Program Studi</td> 
                                                    <td>:</td>
                                                    <td>S1 - Farmasi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>Mikael Silvestre</td>
                                                </tr>
                                                <tr>
                                                    <td>NIM</td>
                                                    <td>:</td>
                                                    <td>11092384</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12 col-xl-12">

                                    <div class="main-card mb-3 card card-border">
                                        <div class="card-body">

                                            <h3 class="text-center mb-4">Formulir Pengajuan Proposal Skripsi</h3>
                                            <hr>

                                            <div class="position-relative row form-group mt-5 mb-5">
                                                <legend class="col-form-label col-sm-12">Judul/Topik Penelitian</legend>
                                                <div class="col-sm-12">
                                                    <ol>
                                                        <li>Pembuatan Obat Herbal Tradisional Kalimantan Timur</li>
                                                        <li>Meneliti Manfaat Kecambah</li>
                                                    </ol>
                                                </div>
                                            </div>

                                            <div class="position-relative row">
                                                <legend class="col-form-label col-sm-12">Dosen Pembimbing</legend>
                                                <div class="col-sm-12">
                                                    <ol>
                                                        <li>Prof. Sardji</li>
                                                        <li>Prof. Lukman Hakim</li>
                                                    </ol>
                                                </div>
                                            </div>

                                            <div class="position-relative row form-check">
                                                <div class="text-center">
                                                    <a class="btn btn-primary" href="sidang-proposal.php"><span class="pe-7s-angle-left"></span> Kembali</a>
                                                </div>
                                            </div>

                                        
                                            
                                        </div>

                                    </div>
                                    
                                    
                                    
                                </div>

                                
                            
                        </div>          

                    </div>

                    <?php include "include/footer.php";?>

                     
